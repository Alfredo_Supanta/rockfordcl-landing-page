import PropTypes from 'prop-types'
import { intlShape, injectIntl } from 'react-intl'

const CustomOrderLink = ({ render, intl }) => {
  return render([
    {
      name: intl.formatMessage({
        id: 'pedidos.message',
        defaultMessage: 'Pedidos',
      }),
      path: '/pedidos',
      class: 'test',
    },
  ])
}

CustomOrderLink.propTypes = {
  render: PropTypes.func.isRequired,
  intl: intlShape.isRequired,
}

export default injectIntl(CustomOrderLink)
