import React, { Fragment } from 'react'
import { Route } from 'vtex.my-account-commons/Router'

import CustomOrders from './CustomOrders'
import CustomDetailOrder from './CustomOrderDetail'

const CustomOrderPage = (props) => {
  return (
    <Fragment>
      <Route exact path="/pedidos" component={CustomOrders} />
      <Route
        exact
        path="/pedidos/:orderId"
        component={CustomDetailOrder}
        route={props}
      />
    </Fragment>
  )
}

export default CustomOrderPage
