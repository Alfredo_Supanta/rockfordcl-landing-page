import React from 'react'

const HideMessageOrderplacedGiftcard = () => {
  setTimeout(() => {
    if (window.location.pathname === '/checkout/orderPlaced/') {
      const productName = document.querySelector(
        '.vtex-order-placed-2-x-productName'
      ).innerText
      const messagesToDelete = [
        'El período de entrega comienza a contar desde el momento en que se confirma el pago.',
        'Se enviará un código de seguimiento a su correo electrónico cuando comience el proceso de entrega.',
      ]
      if (
        window.location.pathname === '/checkout/orderPlaced/' &&
        productName.includes('Gift Card')
      ) {
        const messages = document.querySelectorAll(
          '.vtex-order-placed-2-x-noticeListItem'
        )
        messages.forEach((message) => {
          if (messagesToDelete.indexOf(message.innerText) != -1) {
            message.style.display = 'none'
          }
        })
      }
    }
  }, 1200)

  return null
}

export default HideMessageOrderplacedGiftcard
