import React from 'react'
import './DownloadPrintTermsConditions.global.css'

const DownloadPrintTermsConditions = () => {
  const printPage = (e) => {
    e.preventDefault()
    e.stopPropagation()
    window.print()
  }
  return (
    <div className="download-print">
      <button type="button" onClick={printPage}>
        <img src="/arquivos/print-btn.png" alt="Imprimir" />
      </button>
    </div>
  )
}

export default DownloadPrintTermsConditions
