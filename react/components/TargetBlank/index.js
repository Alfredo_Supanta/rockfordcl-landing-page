import React, { useEffect } from 'react'
import { useProduct } from 'vtex.product-context'

const TargetBlank = () => {
  const itemProduct = useProduct()
  useEffect(() => {
    const trgb = document.getElementById('menu-item-custom-rkf-reference')
    trgb.setAttribute('target', '_self')
    console.log(trgb)
  }, [itemProduct])
  return null
}
export default TargetBlank
