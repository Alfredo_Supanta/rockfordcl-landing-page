/* eslint react/prefer-stateless-function: 0, no-useless-constructor: 0 */
import React from 'react'
import { ProductContext } from 'vtex.product-context'
import './CorebizProductDiscountComponent.global.css'

class CorebizProductDiscountComponent extends React.Component {
  constructor(props) {
    super(props)
  }

  render() {
    const { selectedItem } = this.props
    const { Price, ListPrice } = selectedItem.sellers[0].commertialOffer
    const DiscountPercent =
      Price && ListPrice && Price != ListPrice
        ? 100 - (Price * 100) / ListPrice
        : null

    return (
      <div className="discount-block">
        {DiscountPercent ? (
          <div className="discount_percent">
            <span>{'-' + DiscountPercent.toFixed(0) + '% OFF'}</span>
          </div>
        ) : null}
      </div>
    )
  }
}

const ProductImagesContext = () => {
  const { selectedItem } = React.useContext(ProductContext)

  return <CorebizProductDiscountComponent selectedItem={selectedItem} />
}
export default ProductImagesContext
