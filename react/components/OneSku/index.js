/* eslint no-restricted-globals: 0, react-hooks/exhaustive-deps: 0 */
import React, { useEffect, useState } from 'react'
import { useProduct } from 'vtex.product-context'

const sleep = (mls) => new Promise((c) => setTimeout(c, mls))

function OneSku({ match = '/p$', enabled = true }) {
  let count = 0
  const valuesFromContext = useProduct()
  const [last, SetLast] = useState(undefined)

  useEffect(() => {
    if (!enabled) return
    if (valuesFromContext.product.productId === last) return
    if (valuesFromContext.skuSelector.areAllVariationsSelected) return
    if (!location.pathname.match(new RegExp(match))) return // nosemgrep
    if (!valuesFromContext || valuesFromContext.product.items.length !== 1) {
      SetLast(undefined)
      return
    }

    const start = async () => {
      const list = document.querySelectorAll(
        '.vtex-store-components-3-x-skuSelectorOptionsList > *'
      )
      if (list.length === 0) {
        await sleep(250)
        if (count < 32) start()
        count++
        return
      }

      if (list.length > 1) return
      list[0].click()

      setTimeout(() => {
        if (
          !list[0].classList.contains(
            'vtex-store-components-3-x-skuSelectorItem--selected'
          )
        ) {
          if (count < 32) start()
          count++
          return
        }
        SetLast(valuesFromContext.product.productId)
      }, 250)
    }
    start()
  }, [valuesFromContext.skuSelector.areAllVariationsSelected])

  return null
}

export default OneSku
