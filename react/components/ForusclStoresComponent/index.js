import React, { useState, useEffect, useRef } from 'react'
import { Map, InfoWindow, Marker, GoogleApiWrapper } from 'google-maps-react'
import { useDevice } from 'react-use-device'
import axios from 'axios'
import './ForusclStoresComponent.global.css'

const style = {
  width: '100%',
  height: '100%',
  position: 'relative !important',
}

const Locales = ({ google }) => {
  const [jsonLocales, setJsonLocales] = useState([])
  const [jsonLocalesFiltered, setJsonLocalesFiltered] = useState([])
  const [zoom, setZoom] = useState(7)
  const [mapCenter, setMapCenter] = useState({ lat: 0, lng: 0 })
  const [showingInfoWindow, setShowingInfoWindow] = useState(false)
  const [activeMarker, setActiveMarker] = useState({})
  const [selectedPlace, setSelectedPlace] = useState({})
  const [searchValue, setSearchValue] = useState('')
  const ref = useRef(null)
  const { isMOBILE } = useDevice()

  const handleInputChange = (event) => {
    const term = event.target.value
    setJsonLocalesFiltered([])
    setSearchValue(term)
    setJsonLocalesFiltered(
      jsonLocales.filter(
        (s) =>
          s.name.toLowerCase().includes(term.toLowerCase()) ||
          s.address.street.toLowerCase().includes(term.toLowerCase()) ||
          s.address.city.toLowerCase().includes(term.toLowerCase())
      )
    )
  }

  const setCenter = (lat, lng) => {
    isMOBILE && ref.current.scrollIntoView({ behavior: 'smooth' })
    setZoom(11)
    setMapCenter({
      lat,
      lng,
    })
  }

  const onMarkerClick = (props, marker) => {
    setSelectedPlace(props)
    setActiveMarker(marker)
    setShowingInfoWindow(true)
  }

  const onClose = () => {
    if (showingInfoWindow) {
      setShowingInfoWindow(false)
      setActiveMarker(null)
    }
  }

  const getLocales = () => {
    return axios.get('/files/locales.json').then((res) => {
      const jsonLocalesList = res.data

      //   const indexCenter = res.data.findIndex((local, index) => {
      //     if (local.id == 660) {
      //       return index
      //     }
      //   })

      setMapCenter({
        lat: jsonLocalesList[0].address.location.latitude,
        lng: jsonLocalesList[0].address.location.longitude,
      })
      setJsonLocales(jsonLocalesList)
      setJsonLocalesFiltered(jsonLocalesList)

      return jsonLocalesList
    })
  }

  useEffect(() => {
    getLocales()
  }, [])

  return (
    <div className="container">
      <div id="mapa" className="row">
        <div className="col col-md-6">
          <div className="headerLocales">
            <div className="w-100 col">
              <div className="row nowrap flex-spacebetween header-results">
                <h3>Encuéntranos</h3>
                <label className="searchbar nowrap" htmlFor="search">
                  <input
                    type="search"
                    name="search"
                    placeholder="Buscar"
                    autoComplete="off"
                    value={searchValue}
                    onChange={handleInputChange}
                  ></input>
                </label>
              </div>
            </div>
            <div className="w-100">
              <div className="col col-auto">
                Usted está en: <strong>Capital Federal</strong>
              </div>
            </div>
            <div className="w-100 d-flex">
              <div className="col col-sm-6">
                <h5>Província</h5>
                <select>
                  <option value="Capital Federal">Capital Federal</option>
                </select>
              </div>
              <div className="col col-sm-6">
                <h5>Localidad</h5>
                <select>
                  <option value="Capital Federal">Capital Federal</option>
                </select>
              </div>
            </div>
          </div>
          <div className="listLocales col">
            <ul id="listLocalesFiltered">
              {jsonLocalesFiltered.map((local) => (
                <li
                  className="d-flex flex-wrap"
                  key={`${local.id}-${local.name.trim().toLowerCase()}`}
                  aria-hidden="true"
                  onClick={() =>
                    setCenter(
                      local.address.location.latitude,
                      local.address.location.longitude
                    )
                  }
                >
                  <label
                    className="d-flex w-100"
                    htmlFor={`t-${local.id}-${local.name.trim().toLowerCase()}`}
                  >
                    <input
                      type="radio"
                      id={`t-${local.id}-${local.name.trim().toLowerCase()}`}
                      name="tienda"
                      value={`t-${local.id}-${local.name.trim().toLowerCase()}`}
                    />
                    <div className="d-flex w-100 li-wrapper">
                      <div className="col col-1 icon">
                        <img src="/arquivos/icon-rockford.png" />
                      </div>
                      <div className="col col-11 data">
                        <h3>{local.name}</h3>
                        <p>
                          {local.address.street +
                            ', ' +
                            local.address.number +
                            ' - ' +
                            local.address.neighborhood +
                            ' - ' +
                            local.address.city +
                            ' - ' +
                            local.address.state}
                        </p>
                      </div>
                    </div>
                  </label>
                </li>
              ))}
            </ul>
          </div>
        </div>
        <div ref={ref} className="col col-md-6">
          <Map
            className="map"
            google={google}
            zoom={showingInfoWindow ? 16 : zoom}
            style={style}
            initialCenter={mapCenter}
            center={mapCenter}
          >
            {jsonLocalesFiltered.map((marker) => (
              <Marker
                onClick={onMarkerClick}
                name={marker.name}
                key={`${marker.id}-${marker.name.trim().toLowerCase()}`}
                position={{
                  lat: marker.address.location.latitude,
                  lng: marker.address.location.longitude,
                }}
                addres={
                  marker.address.street +
                  ', ' +
                  marker.address.number +
                  ', ' +
                  marker.address.neighborhood +
                  ', ' +
                  marker.address.state
                }
                icon={{
                  url: '/arquivos/icon-maps.png?v=3',
                }}
              />
            ))}
            <InfoWindow
              marker={activeMarker}
              visible={showingInfoWindow}
              onClose={onClose}
            >
              <div className="infoWindow">
                <h4>
                  <strong>{selectedPlace.name}</strong>
                </h4>
                <p>{selectedPlace.addres}</p>
              </div>
            </InfoWindow>
          </Map>
        </div>
      </div>
    </div>
  )
}

export default GoogleApiWrapper({
  apiKey: 'AIzaSyBMqBY2MgO0zgkQoIrBNE-WQCpY190k-mc',
})(Locales)
