import React, { useEffect } from 'react'

const FormCyber = () => {
  useEffect(() => {
    const script = document.createElement('script')
    script.setAttribute('src', '//js.hsforms.net/forms/v2.js')
    script.setAttribute('type', 'text/javascript')

    document.body.appendChild(script)

    script.addEventListener('load', () => {
      if (window.hbspt) {
        window.hbspt.forms.create({
          region: 'na1',
          portalId: '8157427',
          formId: 'ed7e3ffa-faad-48aa-b3a0-fbfbfeb62a32',
          target: '#formCyber',
        })
      }
    })
  }, [])

  return <div id="formCyber"></div>
}

export default FormCyber
