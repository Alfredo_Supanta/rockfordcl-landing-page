import { useEffect } from 'react'
import './CustomStickyObserver.global.css'

const CustomStickyObserver = () => {
  function handleIntersect(entries) {
    entries.forEach((entry) => {
      const buyButtonsElement = document.querySelector(
        '.vtex-flex-layout-0-x-flexRow--rowBuyButton'
      )
      const buyButtonOriginalPosition = document.querySelector(
        '.buy-now-button-position'
      )
      if (entry.target.isEqualNode(buyButtonOriginalPosition)) {
        if (entry.intersectionRatio === 1) {
          buyButtonOriginalPosition.style.height = 'auto'
          buyButtonsElement.classList.remove('custom-sticky-buy-now-button')
        } else {
          buyButtonOriginalPosition.style.height =
            buyButtonsElement.clientHeight + 20 + 'px'
          buyButtonsElement.classList.add('custom-sticky-buy-now-button')
        }
      }
    })
  }

  function createObserver(element) {
    const options = {
      root: null,
      rootMargin: '0px',
      threshold: 1.0,
    }

    const observer = new IntersectionObserver(handleIntersect, options)
    observer.observe(element)
  }
  useEffect(() => {
    const buyButtonOriginalPositionIsNotCreated =
      document.querySelector('.buy-now-button-position') === null
    if (buyButtonOriginalPositionIsNotCreated) {
      const buyButtonsElement = document.querySelector(
        '.vtex-flex-layout-0-x-flexRow--rowBuyButton'
      )
      buyButtonsElement.insertAdjacentHTML(
        'afterend',
        "<div class='buy-now-button-position'></div>"
      )
      const buyButtonOriginalPosition = document.querySelector(
        '.buy-now-button-position'
      )
      createObserver(buyButtonOriginalPosition)
    }
  })
  return null
}

export default CustomStickyObserver
