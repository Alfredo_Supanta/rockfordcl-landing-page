import React from 'react'
import './CorebizSideMenuComponent.global.css'

class CorebizSideMenuComponent extends React.Component {
  constructor() {
    super()
    this.state = {
      sideMenuActive: '',
    }
  }

  render() {
    return (
      <div className="SideMenu">
        <p>CENTRO DE AYUDA</p>
        <ul>
          <li>
            <a
              onClick={() =>
                this.setState({
                  sideMenuActive:
                    this.state.sideMenuActive === 'preguntas'
                      ? ''
                      : 'preguntas',
                })
              }
            >
              Preguntas Frecuentes
            </a>
            <ul
              className={
                'submenu' +
                (this.state.sideMenuActive === 'preguntas' ? ' open' : '')
              }
            >
              <li>
                <a href="/como-comprar">¿Cómo comprar en RockFord.cl?</a>
              </li>
              <li>
                <a href="/opciones-despacho">
                  ¿Cuáles son las opciones de despacho?
                </a>
              </li>
              <li>
                <a href="/paquetes">
                  ¿Puede mi orden llegar en diferentes paquetes?
                </a>
              </li>
              <li>
                <a href="/orden">¿Cómo se donde está mi orden?</a>
              </li>
              <li>
                <a href="/despacho-tiempo">
                  ¿Cuánto se demora en llegar mi compra? ¿En qué horario?
                </a>
              </li>
              <li>
                <a href="/cambio">
                  ¿Qué pasa si la talla me queda mal o el producto no es lo que
                  esperaba?
                </a>
              </li>
              <li>
                <a href="/como-pagar">¿Cómo puedo pagar?</a>
              </li>
              <li>
                <a href="/productos">
                  ¿Los productos son los mismos que los de las tiendas?
                </a>
              </li>
              <li>
                <a href="/producto-fallado">
                  ¿Qué pasa si mi producto esta fallado?
                </a>
              </li>
              <li>
                <a href="/mails">No me llegan los mails, ¿Qué hago?</a>
              </li>
              <li>
                <a href="/boleta">¿Cómo obtengo la boleta de mi pedido?</a>
              </li>
              <li>
                <a href="/seguimiento">¿Cómo hago seguimiento a mi despacho?</a>
              </li>
            </ul>
          </li>
          <li>
            <a
              onClick={() =>
                this.setState({
                  sideMenuActive:
                    this.state.sideMenuActive === 'cliente' ? '' : 'cliente',
                })
              }
            >
              Cliente Nuevo
            </a>
            <ul
              className={
                'submenu' +
                (this.state.sideMenuActive === 'cliente' ? ' open' : '')
              }
            >
              <li>
                <a href="/beneficios">Beneficios De Rockford</a>
              </li>
              <li>
                <a href="/login">Registrate</a>
              </li>
            </ul>
          </li>
          <li>
            <a
              onClick={() =>
                this.setState({
                  sideMenuActive:
                    this.state.sideMenuActive === 'cuenta' ? '' : 'cuenta',
                })
              }
            >
              Mi Cuenta
            </a>
            <ul
              className={
                'submenu' +
                (this.state.sideMenuActive === 'cuenta' ? ' open' : '')
              }
            >
              <li>
                <a href="/mi-cuenta">¿Qué Puedo Hacer Desde Mi Cuenta?</a>
              </li>
              <li>
                <a href="/login">Olvidé Mi Clave</a>
              </li>
            </ul>
          </li>
          <li>
            <a href="/politica-privacidad">Política De Privacidad</a>
          </li>
          {/* <li>
                        <a onClick={() => this.setState({ sideMenuActive: this.state.sideMenuActive === "seguridad" ? "" : "seguridad" })}>Seguridad Y Privacidad</a>
                        <ul className={"submenu" + (this.state.sideMenuActive === "seguridad" ? " open" : "")}>
                            <li><a href="/compra-segura">Compra Segura</a></li>
                            <li><a href="/politica-privacidad">Política De Privacidad</a></li>
                        </ul>
                    </li> */}
          <li>
            <a href="/tiendas">Nuestras Tiendas</a>
          </li>
          <li>
            <a href="/terminos-condiciones">Términos Y Condiciones</a>
          </li>
          <li>
            <a href="/contacto">Contáctanos</a>
          </li>
        </ul>
      </div>
    )
  }
}
export default CorebizSideMenuComponent
