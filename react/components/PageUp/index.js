/* eslint camelcase: 0, react-hooks/exhaustive-deps: 0 */
import React, { useEffect } from 'react'
import './PageUp.global.css'

const scrollToTop = () => {
  const c = document.documentElement.scrollTop || document.body.scrollTop
  if (c > 0) {
    window.requestAnimationFrame(scrollToTop)
    window.scrollTo(0, c - c / 5)
  }
}
function PageUp({ desktop, mobile, show_y, omit }) {
  const GetButton = (a535) => {
    if (`more` in a535 && Array.isArray(a535.more)) {
      a535.more.find((a532) => {
        if (!window.location.pathname.match(new RegExp(a532.$regex)))
          // nosemgrep
          return false
        Object.assign(a535, a532)
        return true
      })
    }

    const { width, height, bottom, left, right, path, zindex } = a535
    const item = document.createElement('div')
    document.body.appendChild(item)
    item.classList.add('CustomPageUp')
    item.style = `
            position: fixed;

            width: ${width || '50px'};
            height: ${height || '50px'};
            bottom: ${bottom};
            ${left ? `left: ${left};` : ''}
            ${right ? `right: ${right};` : ''}
            background: url(${path});

            z-index: ${zindex || 100};

            background-size: cover;
            transition: 1000ms opacity,250ms transform;
            opacity: 0;
            cursor: pointer;
        `

    item.onclick = () => scrollToTop()

    return item
  }
  useEffect(() => {
    const stg = window.matchMedia('(min-width: 769px)').matches
      ? desktop
      : mobile

    if (omit && omit.find((r) => window.location.pathname.match(new RegExp(r))))
      return

    const bt = GetButton(stg)
    let ints = 0
    const schl = function (e) {
      ints++
      const show = window.scrollY > (show_y || 300)
      if (show) bt.style.bottom = stg.bottom
      bt.style.opacity = show ? '1' : '0'
      const _int = ints
      setTimeout(() => {
        if (_int === ints && !show) bt.style.bottom = '-100px'
      }, 1000)
    }

    window.addEventListener('scroll', schl)

    return () => {
      bt.parentElement.removeChild(bt)
      window.removeEventListener('scroll', schl)
    }
  }, [])

  return null
}

export default PageUp
