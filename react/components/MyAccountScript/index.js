/* eslint camelcase: 0, react-hooks/exhaustive-deps: 0 */
import React, { useState, useEffect } from 'react'
import './MyAccountScript.global.css'

function ScriptAccount() {
  const [haveOrders, setHaveOrders] = useState(false)

  const redirectToSigueTuCompra = (order_id) => {
    fetch('/api/sessions', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: '{"public":{"country":{"value":"."},"postalCode":{"value":"."}}}',
    })
      .then((response) => {
        return response.json()
      })
      .then((data) => {
        const vtexToken = data.sessionToken
        const anchor = document.createElement('a')
        anchor.setAttribute(
          'href',
          `https://www.siguetucompra.cl/compra/${order_id}?vtextoken=${vtexToken}`
        )
        anchor.setAttribute('rel', 'noopener noreferrer')
        anchor.setAttribute('target', '_blank')
        anchor.style.display = 'none'
        document.body.append(anchor)
        setTimeout(() => {
          anchor.click()
        }, 200)
      })
  }

  const daysDifferenceBetweenDates = (date_start, date_end) => {
    if (!date_start || !date_end) return

    if (date_start.getTime() <= date_end.getTime()) {
      const dif =
        Date.UTC(
          date_start.getYear(),
          date_start.getMonth(),
          date_start.getDate(),
          0,
          0,
          0
        ) -
        Date.UTC(
          date_end.getYear(),
          date_end.getMonth(),
          date_end.getDate(),
          0,
          0,
          0
        )

      return Math.abs(dif / 1000 / 60 / 60 / 24)
    }

    return 0
  }

  const appendButton = () => {
    if (haveOrders) {
      return
    }

    setHaveOrders(true)
    const orders = document.querySelectorAll(
      '.vtex-my-orders-app-3-x-orderCard.myo-order-card.w-100.mv7.ba.overflow-hidden.b--muted-4'
    )

    if (orders) {
      orders.forEach((res) => {
        const months = {
          enero: '01',
          febrero: '02',
          marzo: '03',
          abril: '04',
          mayo: '05',
          junio: '06',
          julio: '07',
          agosto: '08',
          septiembre: '09',
          octubre: '10',
          noviembre: '11',
          diciembre: '12',
        }
        const string = ' de '
        const replace = new RegExp(string, 'g')
        const orderID =
          res.childNodes[0].childNodes[2].childNodes[0].textContent
            .replace(' ', '')
            .split('#')[1]
        const linkTag = document.createElement('a')
        const today = new Date(Date.now())
        let orderDate =
          res.childNodes[0].childNodes[0].childNodes[1].textContent
            .replace(replace, ' ')
            .split(' ')
        orderDate = new Date(
          Date.parse(`${months[orderDate[1]]}/${orderDate[0]}/${orderDate[2]}`)
        )

        const diff = daysDifferenceBetweenDates(orderDate, today)
        const status =
          res.firstElementChild.lastElementChild.lastElementChild
            .firstElementChild.firstElementChild.innerText
        if (res.getElementsByClassName('vtex-btn_devolucion').length == 0) {
          if (diff < 120 && status != 'Cancelado') {
            linkTag.innerHTML = 'Hacer un cambio o devolución'
            linkTag.classList.add('vtex-btn_devolucion')
            linkTag.addEventListener('click', (e) => {
              e.preventDefault()
              redirectToSigueTuCompra(orderID)
            })

            res.childNodes[1].childNodes[1].appendChild(linkTag)
          }
        }

        const btnSeguirEnvio = res.querySelector('.btn-seguir-envio')

        if (!btnSeguirEnvio) {
          const sigueTuEnvio = document.createElement('a')

          sigueTuEnvio.innerHTML = 'Sigue tu compra'
          sigueTuEnvio.classList.add(
            'b--action-primary',
            'bg-action-primary',
            '.c-on-action-primary',
            '.t-action',
            'btn-seguir-envio'
          )
          sigueTuEnvio.addEventListener('click', (e) => {
            e.preventDefault()
            redirectToSigueTuCompra(orderID)
          })

          res.childNodes[1].childNodes[1].appendChild(sigueTuEnvio)
        }
      })
    }
  }

  const waitForOrders = () => {
    if (!window.location.href.includes('orders')) {
      return
    }

    if (
      document.querySelectorAll(
        '.vtex-my-orders-app-3-x-orderCard.myo-order-card.w-100.mv7.ba.overflow-hidden.b--muted-4'
      ).length &&
      !document.querySelectorAll('.vtex-btn_devolucion').length
    ) {
      appendButton()
    } else {
      setTimeout(() => {
        waitForOrders()
      }, 1000)
    }
  }

  const removeButton = () => {
    if (
      !document.querySelectorAll('.vtex-btn_devolucion') ||
      document.querySelectorAll('.vtex-btn_devolucion').length
    ) {
      return
    }
    document.querySelectorAll('.vtex-btn_devolucion').forEach((el) => {
      el.remove()
    })
    setHaveOrders(false)
  }

  // oculta url en botón de cancelación
  const hideUrlOrders = () => {
    if (
      window.location.pathname === '/account' &&
      window.location.hash === '#/orders'
    ) {
      setTimeout(() => {
        if (document.querySelector('.vtex-my-orders-app-3-x-cancelBtn')) {
          const cancelOrderBtns = document.querySelectorAll(
            '.vtex-my-orders-app-3-x-cancelBtn'
          )
          cancelOrderBtns.forEach((cancelOrderBtn) => {
            cancelOrderBtn.setAttribute('href', '/')
          })
        }
      }, 1500)
    } else if (
      window.location.pathname === '/account' &&
      window.location.hash.includes('#/orders/rkf')
    ) {
      setTimeout(() => {
        if (document.querySelectorAll('a[href*="/cancel"]')) {
          const cancelOrderBtn = document.querySelector('a[href*="/cancel"]')
          cancelOrderBtn.remove()
        }
      }, 1000)
    }
  }

  useEffect(() => {
    const ordersElement = document.querySelectorAll(
      '.vtex-my-orders-app-3-x-orderCard.myo-order-card.w-100.mv7.ba.overflow-hidden.b--muted-4'
    )
    ordersElement && ordersElement.length ? appendButton() : waitForOrders()

    onhashchange = (hash) => {
      if (!hash.newURL.includes('orders')) {
        removeButton()
        return
      }
      setTimeout(() => {
        const orderDetails = document.querySelector(
          '.vtex-account__page-body.vtex-account__order-details'
        )
        if (orderDetails) {
          // Elementos del DOM que usamos
          const orderDetailsInner = orderDetails.firstElementChild
          const statusBar = orderDetailsInner.lastElementChild
          const orderStatus =
            orderDetailsInner.querySelector('time.c-on-base').firstElementChild
              .firstElementChild
          // Creacion de elementos que van siempre
          const blockButtons = document.createElement('div')
          const textoauxiliar = document.createElement('div')
          textoauxiliar.classList.add(
            'texto-auxiliar',
            'w-100',
            'fl',
            'mt5',
            'mb2-l',
            'mb2-xl'
          )
          blockButtons.classList.add('bloque-botones')
          const btnSeguirEnvio = document.createElement('a')
          const texto = document.createElement('p')
          texto.innerHTML = ''
          // Validación para la fecha de devolución
          const months = {
            enero: '01',
            febrero: '02',
            marzo: '03',
            abril: '04',
            mayo: '05',
            junio: '06',
            julio: '07',
            agosto: '08',
            septiembre: '09',
            octubre: '10',
            noviembre: '11',
            diciembre: '12',
          }
          const string = ' de '
          const replace = new RegExp(string, 'g')
          const today = new Date(Date.now())
          let orderDate = orderDetailsInner
            .querySelector('time.c-on-base')
            .textContent.replace(replace, ' ')
            .split(' ')
          orderDate = new Date(
            Date.parse(
              `${months[orderDate[1]]}/${orderDate[0]}/${orderDate[2]}`
            )
          )

          const diff = daysDifferenceBetweenDates(orderDate, today)

          let orderID = hash.newURL.split('/')
          orderID = orderID[orderID.length - 1] // nosemgrep
          // Chequeo si no es una orden cancelada o expirada
          if (orderStatus.innerText !== 'Cancelado' && diff < 120) {
            // Btn de Devolucion de compra
            texto.innerHTML =
              'Puedes hacer seguimiento a tu compra y solicitar cambios/devoluciones aquí:'
            const btnDevolucion = document.createElement('a')

            btnDevolucion.innerHTML = 'Hacer un cambio o devolución'
            btnDevolucion.classList.add('vtex-btn_devolucion')
            btnDevolucion.addEventListener('click', (e) => {
              e.preventDefault()
              redirectToSigueTuCompra(orderID)
            })

            if (
              document.getElementsByClassName('vtex-btn_devolucion').length == 0
            ) {
              blockButtons.appendChild(btnDevolucion)
            }
          }

          btnSeguirEnvio.innerHTML = 'Sigue tu compra'
          btnSeguirEnvio.classList.add(
            'b--action-primary',
            'bg-action-primary',
            '.c-on-action-primary',
            '.t-action',
            'btn-seguir-envio'
          )
          btnSeguirEnvio.addEventListener('click', (e) => {
            e.preventDefault()
            redirectToSigueTuCompra(orderID)
          })

          // Insertar botones en el DOM
          if (document.getElementsByClassName('btn-seguir-envio').length == 0) {
            blockButtons.appendChild(btnSeguirEnvio)
          }
          if (document.getElementsByClassName('texto-auxiliar').length == 0) {
            textoauxiliar.appendChild(texto)
          }
          console.log(blockButtons)
          orderDetailsInner.insertBefore(textoauxiliar, statusBar)
          orderDetailsInner.insertBefore(blockButtons, statusBar)
        }
      }, 1500)
      waitForOrders()
      setTimeout(() => hideUrlOrders(), 8000)
      hideUrlOrders()
    }
    setTimeout(() => hideUrlOrders(), 8000)
  }, [])

  return null
}

export default ScriptAccount
