import React, { useEffect } from 'react'

const OrderPlacedCustomMessages = () => {
  useEffect(() => {
    setTimeout(() => {
      if (window.location.pathname === '/checkout/orderPlaced/') {
        const changeMessages = () => {
          setTimeout(() => {
            const notices = document.querySelector('[data-testid="notices"]')

            notices.style.display = 'none'

            const oldMessages = [
              'La aprobación del pago puede tardar desde 5 minutos hasta 5 días hábiles.',
              'El período de entrega comienza a contar desde el momento en que se confirma el pago.',
              'Se enviará un código de seguimiento a su correo electrónico cuando comience el proceso de entrega.',
              'El período de recolección en la tienda comienza desde el momento en que se confirma su pago.',
            ]
            const newMessages = [
              'Gracias por comprar en nuestra tienda. Los detalles de tu pedido se encuentran a continuación.',
              'Recibirás un nuevo e-mail cuando los productos estén listos para la entrega.',
              'Recuerda que si compraste más de un producto, estos pueden ser despachados desde distintos orígenes, por lo que podrían llegar por separado y en diferentes días.',
              'Te enviaremos un nuevo e-mail cuando tu pedido esté listo para que lo retires en la tienda.',
            ]

            const children = notices
              ? Array.from(notices.firstChild.children)
              : null

            if (children) {
              children.forEach((child) => {
                switch (child.innerHTML) {
                  case oldMessages[0]:
                    child.innerHTML = newMessages[0]
                    break
                  case oldMessages[1]:
                    child.innerHTML = newMessages[1]
                    break
                  case oldMessages[2]:
                    child.innerHTML = newMessages[2]
                    break
                  case oldMessages[3]:
                    child.innerHTML = newMessages[3]
                    child.style.fontWeight = 600
                    break
                  default:
                    break
                }
              })
            }

            const addressContainer = document.querySelector(
              '[data-testid="address-component"]'
            )
            const store = addressContainer
              ? addressContainer.firstChild.innerHTML.split(' ')[0]
              : null

            if (store === 'Correos') {
              // Caso especial Retiro en Correos
              children.forEach((child) => {
                if (child.innerHTML === newMessages[3]) {
                  child.innerHTML = newMessages[1]
                  child.style.fontWeight = 400

                  const newLi = document.createElement('li')

                  newLi.append(newMessages[2])

                  newLi.classList.add(
                    'vtex-order-placed-2-x-noticeListItem',
                    'pv6',
                    'w-80-ns',
                    'w-90',
                    'center',
                    'c-on-base',
                    'b--muted-4',
                    'bb'
                  )

                  notices.firstChild.append(newLi) // Se crea la nueva li
                }
              })
            }

            notices.style.display = 'block'
          }, 1600)
        }

        changeMessages()
      }
    }, 600)
  }, [])

  return null
}

export default OrderPlacedCustomMessages
