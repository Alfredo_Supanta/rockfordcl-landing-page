import React, { useState } from 'react'
import { ProductContext } from 'vtex.product-context'
import axios from 'axios'
import './CorebizStoreStockComponent.global.css'

const CorebizStoreStockComponent = () => {
  const [selectedItemState, setSelectedItemState] = useState(null)
  const [modalOpen, setModalOpen] = useState(false)
  const [showItem, setShowItem] = useState(null)
  const [stockJson, setStockJson] = useState(null)

  const getStockTiendas = () => {
    setShowItem(selectedItemState || { name: 'Selecciona la talla' })

    if (!selectedItemState.id) {
      return
    }

    axios
      .get(
        'https://woodstock.develop2.forus-sistemas.biz/backend/producto/stock/' +
          selectedItemState.id,
        {
          headers: {
            'Content-Type': 'application/json',
            'x-api-key': 'pkNWVvyVBh8Y6AP5Bd17F8L1Cbx3H0Zo7oNQydqu',
          },
        }
      )
      .then((res) => {
        setStockJson(res.data)
      })
  }

  const productContext = React.useContext(ProductContext)
  const { product, selectedItem } = productContext
  const selectedItemName = selectedItem ? selectedItem.name.split('-')[0] : null
  const productSelect =
    product && product.items
      ? product.items.filter((item) => {
          if (item.name.split('-')[0] == selectedItemName) {
            return item
          }
        })
      : null

  return (
    <div className="vtex-stores-stock">
      <button onClick={() => setModalOpen(true)}>Ver stock en tiendas</button>
      {modalOpen ? (
        <div
          className="vtex-bg-modal"
          onClick={() => setModalOpen(false)}
        ></div>
      ) : null}
      {modalOpen ? (
        <div className="vtex-modal-stores">
          <div className="vtex-header-stores">
            <div className="vtex-skuSelector-stores">
              <div className="vtex-selected-item-img">
                <img src={selectedItem.images[0].imageUrl} />
              </div>
              <div className="vtex-selected-item-details">
                <div
                  className="close-button"
                  style={{ display: 'flex' }}
                  onClick={() => setModalOpen(false)}
                >
                  <span>x</span>
                </div>
                <h3>
                  {selectedItem.nameComplete.replace(selectedItem.name, '')}
                </h3>
                <div className="vtex-selected-item-talles">
                  {productSelect && productSelect.length
                    ? productSelect.map((item) => (
                        <div
                          className={
                            'vtex-btn-stores' +
                            (selectedItemState &&
                            selectedItemState.id &&
                            selectedItemState.id === item.itemId
                              ? ' selected'
                              : '')
                          }
                          onClick={() =>
                            setSelectedItemState({
                              id: item.itemId,
                              name:
                                'Sin resultados para el talle ' +
                                item.name.split('-')[1],
                            })
                          }
                        >
                          <span>{item.name.split('-')[1]}</span>
                        </div>
                      ))
                    : null}
                </div>
                <button
                  className="vtex-button-search-stores"
                  onClick={() => getStockTiendas()}
                >
                  Buscar tiendas
                </button>
              </div>
            </div>
          </div>
          <div className="vtex-body-stores">
            {showItem && showItem.id ? (
              <div className="vtex-result-stores">
                {stockJson &&
                stockJson.sucursales &&
                stockJson.sucursales.length ? (
                  stockJson.sucursales.map((sucursal) => (
                    <div className="vtex-store">
                      <div className="vtex-column-details">
                        <h3>{sucursal.nombre}</h3>
                        <p>{sucursal.direccion}</p>
                        <p>{sucursal.region}</p>
                        <p>{sucursal.comuna}</p>
                        <p>{sucursal.telefono}</p>
                        {sucursal.stock && sucursal.stock.length ? (
                          <div className="vtex-units">
                            {
                              <p
                                className={
                                  sucursal.stock.toLowerCase().includes('pocas')
                                    ? 'vtex-pocas'
                                    : 'vtex-muchas'
                                }
                              >
                                {sucursal.stock}
                              </p>
                            }
                          </div>
                        ) : (
                          <p className="vtex-no-stock">
                            Sin stock en esa tienda
                          </p>
                        )}
                      </div>
                      <div className="vtex-column-map">
                        <a
                          href={
                            'https://www.google.com/maps/search/?api=1&query=' +
                            sucursal.direccion +
                            '&' +
                            sucursal.latitud +
                            ',' +
                            sucursal.longitud
                          }
                          target="_blank"
                          rel="noreferrer"
                        >
                          <img src="/arquivos/icon-maps.png?v=3" />
                          <span>Ver en mapa</span>
                        </a>
                      </div>
                    </div>
                  ))
                ) : (
                  <p className="vtex-no-stock">{stockJson}</p>
                )}
              </div>
            ) : (
              <h3 className="vtex-error-stores">
                {showItem ? showItem.name : null}
              </h3>
            )}
          </div>
        </div>
      ) : null}
    </div>
  )
}

export default CorebizStoreStockComponent
