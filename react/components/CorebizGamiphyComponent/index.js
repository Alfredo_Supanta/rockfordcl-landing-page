import React, { NoSSR, Suspense, Loading, LoginContent } from 'react'

const CorebizGamiphyComponent = (props) => {
  return (
    <div
      id="gamiphy-login-widget"
      style={{
        maxWidth: '43.75rem',
        height: '16rem',
        marginLeft: 'auto',
        marginRight: 'auto',
      }}
    />
  )
}

export default CorebizGamiphyComponent
