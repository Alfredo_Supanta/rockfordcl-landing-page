import React, { useEffect } from 'react'

const VideoPlay = () => {
  useEffect(() => {
    const video = document.querySelector(
      '.vtex-stack-layout-0-x-stackItem--videoEl'
    )
    const btnCover = document.querySelector(
      '.vtex-stack-layout-0-x-stackItem--video-container--btn-play-cover'
    )
    const imagenCover = document.querySelector(
      '.vtex-stack-layout-0-x-stackItem--cover-video'
    )
    const videoContainer = document.querySelector(
      '.vtex-store-video-1-x-videoContainer--videoEl'
    ).childNodes[0]
    btnCover.addEventListener('click', () => {
      video.style.opacity = '1'
      imagenCover.style.display = 'none'
      btnCover.style.display = 'none'
      videoContainer.play()
    })
  }, [])

  return null
}

export default VideoPlay
