import { useProduct } from 'vtex.product-context'

import specificationsCucardaData from './CucardaDataInfo'

const hasOwnProperty = (object, key) =>
  Object.prototype.hasOwnProperty.call(object, key)

export default function useCucardaData() {
  const { product } = useProduct()
  console.log(product)
  if (product && hasOwnProperty(product, 'productClusters')) {
    const { properties } = product
    const logosProperty = properties.find(
      (property) => property.name === 'Logos'
    )
    if (logosProperty && logosProperty.values.length > 0) {
      const logos = logosProperty.values
      return logos.flatMap((logo) =>
        hasOwnProperty(specificationsCucardaData, logo)
          ? specificationsCucardaData[logo]
          : []
      )
    }
  }
  return null
}
