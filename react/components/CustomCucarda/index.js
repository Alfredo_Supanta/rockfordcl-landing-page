import React from 'react'

import useCucardaData from './cucardaDataHook'
import styles from './styles.css'

export default function CustomCucarda() {
  const cucardas = useCucardaData()

  const renderCucardas = () => {
    return cucardas.map((cucarda) => (
      <div className={styles['cucardas-container']}>
        <a href={cucarda.url} className={styles['cucarda-link']}>
          <img className={styles.cucarda} src={cucarda.src}></img>
        </a>
      </div>
    ))
  }

  return cucardas.length > 0 ? <div> {renderCucardas()}</div> : null
}
