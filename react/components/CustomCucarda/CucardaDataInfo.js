const colectionsData = {
  398: {
    src: '/arquivos/WRINKLE.png',
    url: '/398?map=productClusterIds',
  },
  401: {
    src: '/arquivos/TECWOR.png',
    url: '/401?map=productClusterIds',
  },
  405: {
    src: '/arquivos/TECWAX.png',
    url: '/405?map=productClusterIds',
  },
  400: {
    src: '/arquivos/TRAVEL.png',
    url: '/400?map=productClusterIds',
  },
  265: {
    src: '/arquivos/UPF.png',
    url: '/265?map=productClusterIds',
  },
  403: {
    src: '/arquivos/SOFT.png',
    url: '/403?map=productClusterIds',
  },
  355: {
    src: '/arquivos/SEDA.png',
    url: '/355?map=productClusterIds',
  },
  406: {
    src: '/arquivos/ANTI.png',
    url: '/406?map=productClusterIds',
  },
  345: {
    src: '/arquivos/NATURAL.png',
    url: '/345?map=productClusterIds',
  },
  384: {
    src: '/arquivos/LINO.png',
    url: '/384?map=productClusterIds',
  },
  607: {
    src: '/arquivos/LANA.png',
    url: '/lana',
  },
  358: {
    src: '/arquivos/LANAMERINO.png',
    url: '/lana',
  },
  407: {
    src: '/arquivos/THERMORE.png',
    url: '/thermore',
  },
  402: {
    src: '/arquivos/HONEYFOAM.png',
    url: '/402?map=productClusterIds',
  },
  335: {
    src: '/arquivos/algodon_custom_cucarda.png',
    url: '/algodonorganico',
  },
  357: {
    src: '/arquivos/cashmere_custom_cucarda.png',
    url: '/357?map=productClusterIds',
  },
  362: {
    src: '/arquivos/bambu_custom_cucarda.png',
    url: '/362?map=productClusterIds',
  },
  364: {
    src: '/arquivos/hechoamano_custom_cucarda.png',
    url: '/364?map=productClusterIds',
  },
  395: {
    src: '/arquivos/fibras_recicladas_custom_cucarda.png',
    url: '/fibras-recicladas',
  },
  369: {
    src: '/arquivos/crochet_custom_cucarda.png',
    url: '/369?map=productClusterIds',
  },
}

const specificationsCucardaData = {
  'Wrinkle Free': {
    src: '/arquivos/WRINKLE.png',
    url: '/398?map=productClusterIds',
  },
  WOR: {
    src: '/arquivos/TECWOR.png',
    url: '/401?map=productClusterIds',
  },
  WAX: {
    src: '/arquivos/TECWAX.png',
    url: '/405?map=productClusterIds',
  },
  'Travel 360': {
    src: '/arquivos/TRAVEL.png',
    url: '/400?map=productClusterIds',
  },
  UPF: {
    src: '/arquivos/UPF.png',
    url: '/265?map=productClusterIds',
  },
  'Soft Stones': {
    src: '/arquivos/SOFT.png',
    url: '/403?map=productClusterIds',
  },
  Seda: {
    src: '/arquivos/SEDA.png',
    url: '/355?map=productClusterIds',
  },
  RFID: {
    src: '/arquivos/ANTI.png',
    url: '/406?map=productClusterIds',
  },
  'Natural Flex': {
    src: '/arquivos/NATURAL.png',
    url: '/345?map=productClusterIds',
  },
  'Lino Orgánico': {
    src: '/arquivos/LINO.png',
    url: '/384?map=productClusterIds',
  },
  Lana: {
    src: '/arquivos/LANA.png',
    url: '/607?map=productClusterIds',
  },
  'Lana Merino': {
    src: '/arquivos/LANAMERINO.png',
    url: '/358?map=productClusterIds',
  },
  Thermore: {
    src: '/arquivos/THERMORE.png',
    url: '/407?map=productClusterIds',
  },
  'Honey Foam': {
    src: '/arquivos/HONEYFOAM.png',
    url: '/402?map=productClusterIds',
  },
  'Algodón Orgánico': {
    src: '/arquivos/algodon_custom_cucarda.png',
    url: '/335?map=productClusterIds',
  },
  Cashmere: {
    src: '/arquivos/cashmere_custom_cucarda.png',
    url: '/357?map=productClusterIds',
  },
  Bambú: {
    src: '/arquivos/bambu_custom_cucarda.png',
    url: '/362?map=productClusterIds',
  },
  'Hecho a Mano': {
    src: '/arquivos/hechoamano_custom_cucarda.png',
    url: '/364?map=productClusterIds',
  },
  'Fibras Recicladas': {
    src: '/arquivos/fibras_recicladas_custom_cucarda.png',
    url: '/395?map=productClusterIds',
  },
  Crochet: {
    src: '/arquivos/crochet_custom_cucarda.png',
    url: '/369?map=productClusterIds',
  },
}

export default specificationsCucardaData
