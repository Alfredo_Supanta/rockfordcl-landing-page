import React, { useEffect } from 'react'

const HubspotFormFooter = () => {
  useEffect(() => {
    const script = document.createElement('script')
    script.setAttribute('src', '//js.hsforms.net/forms/v2.js')
    script.setAttribute('type', 'text/javascript')

    document.body.appendChild(script)

    script.addEventListener('load', () => {
      if (window.hbspt) {
        window.hbspt.forms.create({
          region: 'na1',
          portalId: '8157427',
          formId: '551c6148-0eb9-465b-9caa-aec519f922c4',
          target: '#hubspotFormFooter',
        })
      }
    })
  }, [])

  return <div id="hubspotFormFooter"> </div>
}

export default HubspotFormFooter
