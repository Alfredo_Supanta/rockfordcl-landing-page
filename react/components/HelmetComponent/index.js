import React from 'react'
import Helmet from 'react-helmet'

class HelmetComponent extends React.Component {
  constructor(props) {
    super(props)

    this.state = {}
  }

  render() {
    return (
      <Helmet>
        <script
          src="https://cdn.onesignal.com/sdks/OneSignalSDK.js"
          async=""
        ></script>
        <script>
          {`
                        var OneSignal = window.OneSignal || [];
                        OneSignal.push(function() {
                          OneSignal.init({
                            appId: "4aa9b106-91ec-4306-871a-f40c98d47fb9",
                          });
                        });
                    `}
        </script>

        <link
          rel="alternate"
          href="https://www.rockford.cl/"
          hrefLang="es-CL"
        />

        {/* <script id="mcjs">{`!function(c,h,i,m,p){m=c.createElement(h),p=c.getElementsByTagName(h)[0],m.async=1,m.src=i,p.parentNode.insertBefore(m,p)}(document,"script","https://chimpstatic.com/mcjs-connected/js/users/1b42ccefb665c9f7076b49ffc/3c0b411816c276cf292ea3259.js");`}</script> */}
      </Helmet>
    )
  }
}
export default HelmetComponent
