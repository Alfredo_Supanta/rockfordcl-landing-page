import { useEffect } from 'react'

const ArrowSlider = () => {
  useEffect(() => {
    const buttonSlider = document.querySelectorAll(
      '.vtex-rich-text-0-x-paragraph--button-rkfmoment'
    )

    buttonSlider.forEach((element) => {
      element.addEventListener('click', function (e) {
        const hrefButton = e.target.childNodes[0].getAttribute('href')
        window.open(hrefButton, '_self')
      })
    })
  }, [])

  return null
}

export default ArrowSlider
