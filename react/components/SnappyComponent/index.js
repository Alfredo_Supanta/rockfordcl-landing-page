import React, { useEffect } from 'react'

const SnappyComponent = () => {
  useEffect(() => {
    const snappyScript = document.createElement('script')
    const windowSize = window.innerWidth

    snappyScript.setAttribute(
      'src',
      'https://api.snappylabs.io/dist/5054411e-84dc-4ea1-8847-80ec8024792a'
    )
    snappyScript.setAttribute('async', '')

    document.body.appendChild(snappyScript)
  }, [])

  return null
}

export default SnappyComponent
