/* eslint radix: 0 */
import React, { useState, useEffect } from 'react'
import { useRuntime } from 'vtex.render-runtime'
import { useSearchPage } from 'vtex.search-page-context/SearchPageContext'
import { path } from 'ramda'
import './LoadingIcon.global.css'

const loadingIconClass = 'lds-dual-ring'
const loadingIconSelector = `.${loadingIconClass}`
const gallerySelector = `.vtex-search-result-3-x-gallery`

const PaginadorPlp = () => {
  const { query, setQuery } = useRuntime()
  const { maxItemsPerPage, searchQuery } = useSearchPage()
  const currentPage = query.page ? query.page : 1
  const fetchMore = path(['fetchMore'], searchQuery)
  const [page, setPage] = useState(currentPage)
  const firstProductId =
    Object.prototype.hasOwnProperty.call(searchQuery, 'products') &&
    typeof searchQuery.products !== 'undefined' &&
    searchQuery.products.length > 0
      ? searchQuery.products[0].productId
      : null

  const fetchNewPage = (newPage) => {
    const to = newPage * maxItemsPerPage - 1
    const from = to - (maxItemsPerPage - 1)
    setPage(newPage)
    loadingNewPage()
    setQuery({ page: newPage })
    fetchMore({
      variables: {
        from,
        to,
      },
      updateQuery: (prevResult, { fetchMoreResult }) => {
        return {
          ...fetchMoreResult,
          productSearch: {
            ...fetchMoreResult.productSearch,
            products: [...fetchMoreResult.productSearch.products],
          },
        }
      },
    }).catch((error) => {
      return { error }
    })
  }

  const loadingNewPage = () => {
    if (document.querySelector(loadingIconSelector) !== null) {
      document.querySelector(gallerySelector).style.display = 'none'
      document.querySelector(loadingIconSelector).style.display = 'block'
    }
    window.scrollTo({
      top: 0,
      behavior: 'smooth',
    })
  }

  useEffect(() => {
    const addIcon = () => {
      if (
        document.querySelector(loadingIconSelector) === null &&
        document.querySelector(gallerySelector) !== null
      ) {
        document
          .querySelector(gallerySelector)
          .insertAdjacentHTML(
            'beforebegin',
            `<div class="lds-dual-ring"></div>`
          )
      }
    }
    addIcon()
  })

  // When product ID is updated we finish loading page
  useEffect(() => {
    const finishedLoadingNewPage = () => {
      if (document.querySelector(loadingIconSelector) !== null) {
        document.querySelector(gallerySelector).style.display = ''
        document.querySelector(loadingIconSelector).style.display = ''
      }
    }
    finishedLoadingNewPage()
  }, [firstProductId])

  const [arrOfCurrButtons, setArrOfCurrButtons] = useState([])

  const paginas = searchQuery.recordsFiltered / maxItemsPerPage
  const redondeo = Math.ceil(paginas)

  useEffect(() => {
    let tempNumberOfButtons = [...arrOfCurrButtons]
    const numOfButtons = []

    for (let i = 1; i <= redondeo; i++) {
      numOfButtons.push(i)
    }
    // Primer Caso: Menor que 6
    if (numOfButtons.length < 6) {
      tempNumberOfButtons = numOfButtons
    }
    // Segundo Caso: Mayor/igual a 1 y menor/igual a 3 1,2 y 3
    else if (currentPage >= 1 && currentPage <= 3) {
      tempNumberOfButtons = [1, 2, 3, 4]
    }
    // Tercer Caso: 4
    else if (currentPage === '4') {
      const sliced = numOfButtons.slice(0, 5)
      tempNumberOfButtons = [...sliced]
    }
    // Cuarto Caso: Mayor a 4 y menor al total de pags y es 8
    else if (
      currentPage > 4 &&
      currentPage < numOfButtons.length &&
      numOfButtons.length === 8
    ) {
      const sliced1 = numOfButtons.slice(currentPage - 3, currentPage)
      const sliced2 = numOfButtons.slice(currentPage, currentPage + 1)

      tempNumberOfButtons = [...sliced1, ...sliced2]
    }
    // Quinto Caso: Mayor a 4 y menor al total y menor/igual a 10
    else if (
      currentPage > 4 &&
      currentPage < numOfButtons.length &&
      numOfButtons.length <= 10
    ) {
      const sliced1 = numOfButtons.slice(currentPage - 2, currentPage)
      const sliced2 = numOfButtons.slice(currentPage, numOfButtons.length - 1)

      tempNumberOfButtons = [...sliced1, ...sliced2]
    }
    // Sexto Caso: Mayor a 4 y menor al total y mayor a 10
    else if (
      currentPage > 4 &&
      currentPage < numOfButtons.length - 1 &&
      numOfButtons.length > 10
    ) {
      const sliced1 = numOfButtons.slice(currentPage - 3, currentPage)
      const restOfNumber = numOfButtons.length - 1 - currentPage
      const restOfPage = []
      if (restOfNumber > 0) {
        restOfPage.push(parseInt(currentPage) + 1)
      }
      if (restOfNumber > 1) {
        restOfPage.push(parseInt(currentPage) + 2)
      }
      tempNumberOfButtons = [...sliced1, ...restOfPage]
    }
    // Septimo Caso: Pags totales menos 3
    else if (currentPage > numOfButtons.length - 3) {
      const sliced = numOfButtons.slice(numOfButtons.length - 4)

      tempNumberOfButtons = [...sliced]
    }

    setArrOfCurrButtons(tempNumberOfButtons)
  }, [arrOfCurrButtons, currentPage, redondeo])

  return (
    <div className="paginadorContainer">
      {currentPage > 5 && (
        <button
          className="arrow-paginador-left-doble"
          onClick={() => fetchNewPage(page - 5)}
        >
          {' '}
          left{' '}
        </button>
      )}
      {currentPage > 1 && (
        <button
          className="arrow-paginador-left"
          onClick={() => fetchNewPage(page - 1)}
        >
          {' '}
          left{' '}
        </button>
      )}
      {arrOfCurrButtons.map((data, index) => {
        return (
          <span
            className={data === '...' ? 'clase-puntos' : 'buttons-pages'}
            key={index}
          >
            <button
              className={
                parseInt(currentPage) === data ? 'active-button-page' : ''
              }
              id={data}
              onClick={() => fetchNewPage(data)}
            >
              {data}
            </button>
          </span>
        )
      })}
      {currentPage < redondeo && (
        <button
          className="arrow-paginador-right"
          onClick={() => fetchNewPage(page + 1)}
        >
          {' '}
          right{' '}
        </button>
      )}
      {currentPage < redondeo - 5 && (
        <button
          className="arrow-paginador-right-doble"
          onClick={() => fetchNewPage(page + 5)}
        >
          {' '}
          right{' '}
        </button>
      )}
    </div>
  )
}

export default PaginadorPlp
