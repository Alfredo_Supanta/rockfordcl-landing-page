import React, { useEffect } from 'react'

const VideoClick = () => {
  useEffect(() => {
    if (
      document.querySelectorAll(
        '.vtex-slider-layout-0-x-sliderArrows--carousel-click-home'
      )
    ) {
      const flechas = document.querySelectorAll(
        '.vtex-slider-layout-0-x-sliderArrows--carousel-click-home'
      )
      flechas.forEach((flecha) => {
        flecha.addEventListener('click', function (e) {
          setTimeout(() => {
            if (
              document.querySelector(
                '.vtex-store-video-1-x-videoElement--carousel-click-home'
              )
            ) {
              const video = document.querySelector(
                '.vtex-store-video-1-x-videoElement--carousel-click-home'
              )
              video.addEventListener('click', () => {
                // window.open('https://www.rockford.cl/395?map=productClusterIds', '_self');
              })
            }
          }, 200)
        })
      })
    }
  }, [])

  return null
}

export default VideoClick
