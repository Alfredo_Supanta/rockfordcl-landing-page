import React, { useEffect } from 'react'

const HideUrlCancelOrder = () => {
  const hideUrlOrderPlaced = () => {
    if (window.location.pathname === '/checkout/orderPlaced/') {
      setTimeout(() => {
        if (
          document.querySelector('.vtex-order-placed-2-x-cancelOrderButton')
        ) {
          const cancelOrderBtn = document.querySelector(
            '.vtex-order-placed-2-x-cancelOrderButton'
          ).childNodes[0]
          cancelOrderBtn.setAttribute('href', '/')
        }
      }, 1500)
    }
  }

  useEffect(() => {
    hideUrlOrderPlaced()
  })

  return null
}

export default HideUrlCancelOrder
