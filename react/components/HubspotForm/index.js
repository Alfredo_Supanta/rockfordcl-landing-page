import React, { useEffect } from 'react'

const HubspotForm = () => {
  useEffect(() => {
    const script = document.createElement('script')
    script.setAttribute('src', '//js.hsforms.net/forms/v2.js')
    script.setAttribute('type', 'text/javascript')

    document.body.appendChild(script)

    script.addEventListener('load', () => {
      if (window.hbspt) {
        window.hbspt.forms.create({
          region: 'na1',
          portalId: '8157427',
          formId: 'c9eb4db3-7076-4278-9817-cea6ace596f8',
          target: '#hubspotForm',
        })
      }
    })
  }, [])

  return <div id="hubspotForm"></div>
}

export default HubspotForm
