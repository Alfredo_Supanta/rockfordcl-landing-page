/* eslint react/prefer-stateless-function: 0 */
import React, { Component } from 'react'

class HubspotScript extends Component {
  render() {
    return (
      <script
        type="text/javascript"
        id="hs-script-loader"
        async
        defer
        src="//js.hs-scripts.com/8157427.js"
      ></script>
    )
  }
}

export default HubspotScript
