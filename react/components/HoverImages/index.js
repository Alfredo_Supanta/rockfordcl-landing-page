/* eslint no-restricted-globals: 0 */
import React, { useEffect } from 'react'

function HoverImage() {
  useEffect(() => {
    let cols = []
    let prefix = ''
    const pathname = location.pathname

    switch (pathname) {
      case '/greenhouse':
        prefix = 'gh'
        break
      case '/functionality':
        prefix = 'fn'
        break
      case '/handcrafted':
        prefix = 'hc'
        break
      default:
        break
    }

    const rows = document.querySelectorAll(`[class$="${prefix}-row"]`) // Selecciono todas las filas

    rows.forEach((row) => {
      // Por cada fila...
      const imagesList = []
      const aux = []

      cols = row.querySelectorAll('.vtex-flex-layout-0-x-flexColChild') // Selecciono el contenedor de las imágenes

      cols.forEach((img) => {
        imagesList.push(
          img.querySelector('.vtex-store-components-3-x-imageElement').src
        ) // Obtengo el src de cada imagen
        aux.push(img) // La agrego al arreglo auxiliar para llamar a la fila correcta
      })

      for (let i = 0; i < cols.length; i++) {
        if (i % 2 == 0) {
          cols[i].addEventListener('mouseenter', () => {
            aux[i].querySelector(
              '.vtex-store-components-3-x-imageElement'
            ).src = imagesList[i + 1]
          })

          cols[i].addEventListener('mouseleave', () => {
            aux[i].querySelector(
              '.vtex-store-components-3-x-imageElement'
            ).src = imagesList[i] // nosemgrep
          })
        }
      }
    })
  }, [])

  return null
}

export default HoverImage
