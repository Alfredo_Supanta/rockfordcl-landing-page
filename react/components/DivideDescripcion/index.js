import React, { useEffect } from 'react'
import { useProduct } from 'vtex.product-context'

const DivideDescripcion = () => {
  const itemProduct = useProduct()

  useEffect(() => {
    // descripción first col sólo si tiene hijos
    if (
      document.querySelector('.vtex-flex-layout-0-x-flexColChild--first-col')
        .childElementCount !== 0
    ) {
      const firstColChilds = document.querySelector(
        '.vtex-flex-layout-0-x-flexColChild--first-col'
      ).childNodes[0].childNodes[1].childNodes[0].childNodes

      // console.log(firstColChilds, "primero")
      // limpia elementos de la primera columna
      if (!firstColChilds) return null
      firstColChilds.forEach((child) => {
        if (!child) return null
        // oculta el titulo y lo que sigue del titulo (descripcion)
        if (child.tagName === 'H3') {
          if (
            child.textContent !== 'Características' &&
            child.previousSibling !== child
          ) {
            child.classList.add('hidden')
            child.nextElementSibling.classList.add('hidden')
          }

          if (
            child.textContent === '\n    Características\n' &&
            child.previousSibling.nodeType === 3
          ) {
            child.classList.remove('hidden')
            child.nextElementSibling.classList.remove('hidden')
          }

          if (
            child.textContent === 'Características: ' &&
            child.previousSibling.nodeType === 3
          ) {
            child.classList.remove('hidden')
            child.nextElementSibling.classList.remove('hidden')
          }

          if (
            child.textContent === ' ' &&
            child.previousSibling.nodeType === 3
          ) {
            child.classList.remove('hidden')
            child.nextElementSibling.classList.remove('hidden')
          }
          // ajuste descripción giftcard
          /*  if (child.textContent === "Recuerda:" && child.previousSibling.nodeType === 3) {
                        child.classList.remove("hidden");
                        child.nextElementSibling.classList.remove("hidden");
                    } */
          // giftcard
          if (child.textContent === 'Gift Card Día de la Madre:') {
            child.classList.remove('hidden')
            child.nextElementSibling.classList.remove('hidden')
          }
        }

        if (child.tagName === 'H2') {
          if (
            child.textContent !== 'Características' &&
            child.previousSibling !== null
          ) {
            child.classList.add('hidden')
            child.nextElementSibling.classList.add('hidden')
          }

          if (
            child.textContent === 'Características: ' &&
            child.previousSibling.nodeType === 3
          ) {
            child.classList.remove('hidden')
            child.nextElementSibling.classList.remove('hidden')
          }
        }

        // oculta las imagenes [10].childNodes[1].tagName
        if (child.tagName === 'P') {
          if (!child.childNodes[0]) return null
          if (!child.children[0]) return null
          if (child.childNodes[0].tagName === 'IMG') {
            child.classList.add('hidden')
          }

          if (child.children.length > 0) {
            if (child.children[0].tagName === 'IMG') {
              child.classList.add('hidden')
            }
          }
        }

        // oculta video
        if (
          child.tagName === 'DIV' &&
          child.tagName !== 'imagen-descripcion-fabricacion'
        ) {
          child.classList.add('hidden')
        }

        if (child.tagName === 'imagen-descripcion-fabricacion') {
          child.classList.add('hidden')
        }
      })
    }

    // descripcion second col solo si tiene hijos
    if (
      document.querySelector('.vtex-flex-layout-0-x-flexColChild--second-col')
        .childElementCount !== 0
    ) {
      // oculta el titulo descripcion del producto
      document
        .querySelector(
          '.vtex-flex-layout-0-x-flexColChild--second-col .vtex-store-components-3-x-productDescriptionTitle'
        )
        .classList.add('hidden')
      // si tiene más de un hijo
      if (
        document.querySelector('.vtex-flex-layout-0-x-flexColChild--second-col')
          .childNodes[0].childNodes[1].childNodes[0].childElementCount > 1
      ) {
        document.querySelector(
          '.vtex-flex-layout-0-x-flexColChild--second-col'
        ).parentElement.parentElement.style.width = '50%'
        document.querySelector(
          '.vtex-flex-layout-0-x-flexColChild--first-col'
        ).parentElement.parentElement.style.width = '50%'
        const secondColChilds = document.querySelector(
          '.vtex-flex-layout-0-x-flexColChild--second-col'
        ).childNodes[0].childNodes[1].childNodes[0].childNodes

        // limpia elementos de la segunda columna
        setTimeout(() => {
          if (!secondColChilds) return null

          secondColChilds.forEach((child) => {
            if (!child) return null
            // elimina el video
            setTimeout(() => {
              if (
                secondColChilds[0].className.includes(
                  'responsive-embed-youtube'
                )
              ) {
                secondColChilds[0].remove()
              } else if (child.tagName === 'IFRAME') {
                child.remove()
              }
            }, 200)
            // P
            if (child.tagName === 'P') {
              if (child.previousSibling !== null) {
                if (child.previousSibling.tagName !== 'H3') {
                  child.classList.add('hidden')
                }

                // IMG
                if (child.childNodes[0].tagName === 'IMG') {
                  child.classList.remove('hidden')
                  child.childNodes[0].className = 'imgmodificada'
                  // agrega la clase a al parrafo donde sae encuentra la imagen
                  if (
                    child.tagName === 'P' &&
                    child.childNodes[0].tagName === 'IMG' &&
                    child.previousSibling.nodeType === 3
                  ) {
                    child.setAttribute('id', 'divContenedorImg')
                  } else if (
                    child.tagName === 'P' &&
                    child.childNodes[0].tagName === 'IMG' &&
                    child.previousSibling.tagName === 'UL'
                  ) {
                    child.setAttribute('id', 'divContenedorImg')
                  }

                  // mete las imagenes dentro del primer parrafo que contenga una imagen
                  if (document.getElementById('divContenedorImg')) {
                    const ContenedorImgs =
                      document.getElementById('divContenedorImg')

                    ContenedorImgs.appendChild(child.childNodes[0])
                  }
                }

                if (child.children.length > 0) {
                  if (child.children[0].tagName === 'IMG') {
                    child.classList.remove('hidden')
                    child.children[0].className = 'imgmodificada'
                    // agrega la clase a al parrafo donde sae encuentra la imagen
                    if (
                      child.tagName === 'P' &&
                      child.children[0].tagName === 'IMG' &&
                      child.previousSibling.nodeType === 3
                    ) {
                      child.setAttribute('id', 'divContenedorImg')
                    } else if (
                      child.tagName === 'P' &&
                      child.children[0].tagName === 'IMG' &&
                      child.previousSibling.tagName === 'UL'
                    ) {
                      child.setAttribute('id', 'divContenedorImg')
                    }

                    // mete las imagenes dentro del primer parrafo que contenga una imagen
                    if (document.getElementById('divContenedorImg')) {
                      const ContenedorImgs =
                        document.getElementById('divContenedorImg')

                      ContenedorImgs.appendChild(child.children[0])
                    }
                  }
                }
              } else {
                // oculat el primer p
                child.classList.add('hidden')
              }
            }

            // H2 y H3
            if (child.tagName === 'H3' || child.tagName === 'H2') {
              if (
                child.textContent === 'Características' ||
                child.textContent === 'Características: '
              ) {
                child.classList.add('hidden')
                child.nextElementSibling.classList.add('hidden')
              }

              if (child.textContent !== 'Tejido') {
                child.classList.add('hidden')
                child.nextElementSibling.classList.add('hidden')
              }

              if (child.textContent === 'Tecnología') {
                child.classList.remove('hidden')
                child.nextElementSibling.classList.remove('hidden')
              }

              // giftcard
              if (child.textContent === 'Recuerda:') {
                child.classList.remove('hidden')
                child.nextElementSibling.classList.remove('hidden')
              }
            }

            // UL
            if (child.tagName === 'UL') {
              child.classList.add('hidden')
              if (child.previousElementSibling === null) {
                child.classList.add('hidden')
              } else {
                if (
                  child.previousElementSibling.tagName === 'H2' &&
                  child.previousElementSibling.textContent === 'Tejido'
                ) {
                  child.classList.remove('hidden')
                }

                if (
                  child.previousElementSibling.tagName === 'H3' &&
                  child.previousElementSibling.textContent === 'Tecnología'
                ) {
                  child.classList.remove('hidden')
                }

                if (
                  child.previousElementSibling.tagName === 'H3' &&
                  child.previousElementSibling.textContent === 'Recuerda:'
                ) {
                  child.classList.remove('hidden')
                }
                // if (
                //   child.previousElementSibling.tagName === 'H3' &&
                //   child.previousElementSibling.textContent === 'Gift Card Día de la Madre:'
                // ) {
                //   console.log("dentro")
                //   child.classList.remove('hidden')
                // }
              }
            }

            if (child.tagName === 'imagen-descripcion-fabricacion') {
              child.classList.remove('hidden')
            }
          })
        }, 100)
      }

      // si tiene un solo hijo
      if (
        document.querySelector('.vtex-flex-layout-0-x-flexColChild--second-col')
          .childNodes[0].childNodes[1].childNodes[0].childElementCount === 1
      ) {
        const secondColChilds = document.querySelector(
          '.vtex-flex-layout-0-x-flexColChild--second-col'
        ).childNodes[0].childNodes[1].childNodes[0].childNodes

        const secondCol = document.querySelector(
          '.vtex-flex-layout-0-x-flexColChild--second-col'
        )

        if (!secondColChilds) return null

        // console.log("aqui toy")
        if (secondColChilds[0].tagName == 'UL') {
          // oculta el elemento
          secondColChilds[0].classList.add('hidden')
          secondCol.parentElement.parentElement.style.width = '0'
          // full width first col
          secondCol.parentElement.parentElement.previousElementSibling.style.width =
            '100%'
        }
      }
    }
  }, [itemProduct])

  return null
}

export default DivideDescripcion
