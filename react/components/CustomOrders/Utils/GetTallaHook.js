import { useState, useEffect } from 'react'

import { getVariations } from './Service'

const GetTalla = (product) => {
  const { skuName } = product
  const [talla, setTalla] = useState()
  useEffect(() => {
    const fetchTalla = async () => {
      try {
        const data = await getVariations(product.productId)
        data.skus.forEach((sku) => {
          if (skuName === sku.skuname) {
            if (Object.prototype.hasOwnProperty.call(sku.dimensions, 'Talla')) {
              setTalla(sku.dimensions.Talla)
            }
          }
        })
        if (typeof talla === 'undefined') throw 'La talla ha sido descontinuada'
      } catch (e) {
        // Fallback for products that are not available on the store or for sizes that are not available at the moment
        const optionsArray = skuName.split('-')
        const tallaFromSku = optionsArray[optionsArray.length - 1]
        if (tallaFromSku.length > 5) {
          // If the sku is not separated by "-" properly then we check for spaces
          const lastSpaceArray = skuName.split(' ')
          const lastSpacesFromSku = lastSpaceArray[lastSpaceArray.length - 1]
          const formattedSize = formatSize(lastSpacesFromSku)
          setTalla(formattedSize)
        } else {
          const formattedSize = formatSize(tallaFromSku)
          setTalla(formattedSize)
        }
      }
    }

    const formatSize = (size) => {
      let formattedSize = size
      if (size[0] === '0' && isNumber(size)) {
        formattedSize = parseInt(size, 10) / 10
      }
      return formattedSize
    }

    const isNumber = (value) => !Number.isNaN(Number(value))

    fetchTalla()
  })

  return talla
}

export default GetTalla
