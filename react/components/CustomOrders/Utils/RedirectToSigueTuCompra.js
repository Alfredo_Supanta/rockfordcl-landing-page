const redirectToSigueTuCompra = async (orderId) => {
  const response = await fetch('/api/sessions', {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: '{"public":{"country":{"value":"."},"postalCode":{"value":"."}}}',
  })
  const data = await response.json()
  const vtexToken = data.sessionToken
  // To avoid security issues we add anchor tag instead of navigation with window
  const anchor = document.createElement('a')
  anchor.setAttribute(
    'href',
    `https://www.siguetucompra.cl/compra/${orderId}?vtextoken=${vtexToken}`
  )
  anchor.setAttribute('rel', 'noopener noreferrer')
  anchor.setAttribute('target', '_blank')
  anchor.style.display = 'none'
  document.body.append(anchor)
  setTimeout(() => {
    anchor.click()
  }, 200)
}

export default redirectToSigueTuCompra
