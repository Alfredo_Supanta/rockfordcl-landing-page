const settings = {
  year: 'numeric',
  month: 'long',
  day: '2-digit',
}

export default settings
