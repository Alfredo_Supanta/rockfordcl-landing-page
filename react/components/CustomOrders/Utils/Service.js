const { origin } = window.location
const ordersApiUrl = `${origin}/api/oms/user/orders`
const productVariationsApiUrl = `${origin}/api/catalog_system/pub/products/variations`
const sellerApiUrl = `${origin}/api/catalog_system/pvt/sellers`

export const getOrders = async () => {
  const data = await getData(ordersApiUrl)
  return data
}

export const getVariations = async (productId) => {
  const data = await getData(`${productVariationsApiUrl}/${productId}`)
  return data
}

export const getFullDataOfOrders = async (orders) => {
  const ordersData = []
  for await (const order of orders) {
    const orderData = await getData(`${ordersApiUrl}/${order.orderId}`)
    ordersData.push(orderData)
  }
  return ordersData
}

export const getFullDataOfOrder = async (orderId) => {
  const orderData = await getData(`${ordersApiUrl}/${orderId}`)
  return orderData
}

export const getSeller = async (sellerId) => {
  const sellerData = await getData(`${sellerApiUrl}/${sellerId}`)
  return sellerData
}

const getData = async (url) => {
  const response = await fetch(url)
  const data = await response.json()
  return data
}
