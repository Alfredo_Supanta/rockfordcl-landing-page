import React from 'react'
import './LoadingIcon.global.css'

const LoadingIcon = () => {
  return (
    <div className="lds-ring">
      <div></div>
      <div></div>
      <div></div>
      <div></div>
    </div>
  )
}

export default LoadingIcon
