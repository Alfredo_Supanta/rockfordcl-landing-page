import React from 'react'
import { FormattedDate } from 'react-intl'

import settings from '../../../Utils/DateSettings'

const DeliveryInfo = ({ logisticInfo }) => {
  return (
    <div className="flex flex-column">
      <div className="mw-100  myo-margin-right">
        <h2 className="f4 mb0 lh-copy">Paquete</h2>
      </div>
      <div className="flex flex-column flex-row-l">
        <div>
          <span className="mr3">
            Entregue hasta el{' '}
            <FormattedDate
              value={logisticInfo.shippingEstimateDate}
              year={settings.year}
              month={settings.month}
              day={settings.day}
            ></FormattedDate>
          </span>
          <span className="dib br2 pv2 mt2 ph3 f7 f6-xl fw5 nowrap bg-muted-1 c-on-muted-1">
            {logisticInfo.selectedSla}
          </span>
        </div>
        <div className="w-40-l w-40-xl w-100-m w-100-s pl6-l"></div>
      </div>
    </div>
  )
}

export default DeliveryInfo
