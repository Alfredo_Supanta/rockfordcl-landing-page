import React from 'react'

import DeliveryInfo from './DeliveryInfo/DeliveryInfo'
import ProductTable from './ProductTable/ProductTable'

const PackageInfo = ({ order }) => {
  const logisticInfo = order.shippingData.logisticsInfo[0]
  return (
    <div className="w-100 pv7 fl">
      <DeliveryInfo logisticInfo={logisticInfo}></DeliveryInfo>
      <ProductTable order={order}></ProductTable>
    </div>
  )
}

export default PackageInfo
