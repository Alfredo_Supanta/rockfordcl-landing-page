import React, { useState, useEffect } from 'react'
import { FormattedNumber } from 'react-intl'

import GetTalla from '../../../../Utils/GetTallaHook'
import { getSeller } from '../../../../Utils/Service'

const ProductRow = ({ product, order }) => {
  const talla = GetTalla(product)
  const [seller, setSeller] = useState('')
  useEffect(() => {
    const fetchVendor = async () => {
      const sellerData = await getSeller(product.seller)
      setSeller(sellerData.Name)
    }

    fetchVendor()
  })

  return (
    <tr className="vtex-my-orders-app-3-x-productRow myo-product-row bt b--muted-5 bw1">
      <td className="vtex-my-orders-app-3-x-productInfo myo-product-info pv3 tl v-top overflow-hidden">
        <img
          className="vtex-my-orders-app-3-x-productImage myo-product-image mw4-ns fl mr5"
          src={product.imageUrl}
          alt={product.name}
          crossOrigin="anonymous"
        />
        <div className="fl overflow-hidden w-80-ns lh-copy">
          <a
            target="_blank"
            href={product.detailUrl}
            className="vtex-my-orders-app-3-x-productName myo-product-name fw7 f6 mb0 mt0"
            rel="noreferrer"
          >
            {product.name}
          </a>
          <span className="vtex-my-orders-app-3-x-sellerName myo-seller-name f7 c-muted-1 db">
            {seller}
          </span>
          <span className="vtex-my-orders-app-3-x-sellerName myo-seller-name f7 c-muted-1 db">
            {talla && <>Talla: {talla}</>}
          </span>
        </div>
      </td>
      <td className="vtex-my-orders-app-3-x-productPrice myo-product-price pl2-ns pt3 tl v-top dn dtc-ns">
        <FormattedNumber
          currency={order.storePreferencesData.currencyCode}
          style="currency"
          value={product.sellingPrice / 100}
        />
      </td>
      <td className="vtex-my-orders-app-3-x-productQuantity myo-product-quantity pl2-ns pt3 tl v-top">
        {product.quantity} {product.measurementUnit}
      </td>
      <td className="vtex-my-orders-app-3-x-productTotalPrice myo-product-total-price pl1-l pt3 tl v-top">
        <FormattedNumber
          currency={order.storePreferencesData.currencyCode}
          style="currency"
          value={(product.sellingPrice * product.unitMultiplier) / 100}
        />
      </td>
    </tr>
  )
}

export default ProductRow
