import React from 'react'

import ProductRow from './ProductRow/ProductRow'

const ProductTable = ({ order }) => {
  const { items } = order
  const logisticsInfoList = order.shippingData.logisticsInfo
  return (
    <table className="vtex-my-orders-app-3-x-productTable myo-product-table table w-100 mt7">
      <thead>
        <tr className="tl">
          <th className="pa0 w-60 w-50-ns pb3">Producto</th>
          <th className="pa0 w-20 pl2 pr2 pb3 dn dtc-ns">Precio</th>
          <th className="pa0 w-20 pl2 pr2 pb3">Cantidad</th>
          <th className="pa0 w-30 w-20-ns pb3">Subtotal</th>
        </tr>
      </thead>
      <tbody>
        {items.map((product, index) => {
          product.skuName = order.itemMetadata.Items[index].SkuName
          return (
            <ProductRow
              product={product}
              logisticInfo={logisticsInfoList[index]}
              order={order}
              key={product.productId}
            ></ProductRow>
          )
        })}
      </tbody>
    </table>
  )
}

export default ProductTable
