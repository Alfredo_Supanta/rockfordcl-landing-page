import React, { useEffect, useState } from 'react'
import { ContentWrapper } from 'vtex.my-account-commons'

import { getFullDataOfOrder } from '../Utils/Service'
import OrderInfo from './OrderInfo/OrderInfo'
import PackageInfo from './PackageInfo/PackageInfo'
import AuxButtons from './AuxButtons/AuxButtons'
import LoadingIcon from '../LoadingIcon/LoadingIcon'
import '../CustomOrder.global.css'

const CustomDetailOrder = (props) => {
  const orderId = props.match.params.orderId
  const [order, setOrder] = useState()
  const [isLoading, setIsLoading] = useState(false)

  useEffect(() => {
    const fecthOrderData = async () => {
      if (!order) {
        setIsLoading(true)
        const dataOrder = await getFullDataOfOrder(orderId)
        setIsLoading(false)
        setOrder(dataOrder)
      }
    }

    fecthOrderData()
  }, [order, orderId])

  return (
    <ContentWrapper titleId={`Pedido #${orderId}`} namespace="pedidos">
      {() => (
        <main className="vtex-account__page-body undefined vtex-account__order-details w-100 pa4-s">
          <div className="center w-100">
            {isLoading && <LoadingIcon></LoadingIcon>}
            {order && (
              <>
                <OrderInfo order={order}></OrderInfo>
                <AuxButtons order={order}></AuxButtons>
                <PackageInfo order={order}></PackageInfo>
              </>
            )}
          </div>
        </main>
      )}
    </ContentWrapper>
  )
}

export default CustomDetailOrder
