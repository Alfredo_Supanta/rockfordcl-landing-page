import React from 'react'

import FollowOrder from '../../Buttons/FollowOrder/FollowOrder'
import ReturnOrder from '../../Buttons/ReturnOrder/ReturnOrder'

const AuxButtons = ({ order }) => {
  return (
    <>
      <div className="texto-auxiliar w-100 fl mt5 mb2-l mb2-xl">
        <p>
          Puedes hacer seguimiento a tu compra y solicitar cambios/devoluciones
          aquí:
        </p>
      </div>
      <div className="bloque-botones">
        <ReturnOrder order={order}></ReturnOrder>
        <FollowOrder order={order}></FollowOrder>
      </div>
    </>
  )
}

export default AuxButtons
