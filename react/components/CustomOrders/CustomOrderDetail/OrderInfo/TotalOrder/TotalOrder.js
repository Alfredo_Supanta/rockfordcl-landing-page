import React from 'react'
import { FormattedNumber } from 'react-intl'

const TotalOrder = ({ order }) => {
  const { totals } = order
  return (
    <article className="w-100 fl w-third-m mb5">
      <section className="pa5 ba bw1 b--muted-5 overflow-y-scroll bg-base h4-plus">
        <h3 className="c-on-base mt2 mb5 tracked-mega lh-solid ttu f6">
          Resumen
        </h3>
        <div className="cf w-100">
          <div className="dib f6 fw4 c-on-base w-60">Subtotal</div>
          <div className="dib f6 fw4 c-on-base w-40 tr">
            <FormattedNumber
              currency={order.storePreferencesData.currencyCode}
              style="currency"
              value={totals[0].value / 100}
            />
          </div>
          <hr className="bt-0 bb bw1 b--muted-5 w-100 mb2 mt2" />
        </div>
        <div className="cf w-100">
          <div className="dib f6 fw4 c-on-base w-60">Descuentos</div>
          <div className="dib f6 fw4 c-on-base w-40 tr">
            <FormattedNumber
              currency={order.storePreferencesData.currencyCode}
              style="currency"
              value={totals[1].value / 100}
            />
          </div>
          <hr className="bt-0 bb bw1 b--muted-5 w-100 mb2 mt2" />
        </div>
        <div className="cf w-100 mb7">
          <div className="dib fl f6 fw5 c-on-base w-50">Total</div>
          <div className="dib fr f6 fw5 c-on-base w-50 tr">
            <FormattedNumber
              currency={order.storePreferencesData.currencyCode}
              style="currency"
              value={order.value / 100}
            />
          </div>
        </div>
      </section>
    </article>
  )
}

export default TotalOrder
