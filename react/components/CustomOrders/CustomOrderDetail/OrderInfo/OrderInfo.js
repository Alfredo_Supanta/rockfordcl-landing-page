import React from 'react'
import { FormattedDate } from 'react-intl'

import settings from '../../Utils/DateSettings'
import RepeatOrder from '../../Buttons/RepeatOrder/RepeatOrder'
import Transactions from './Transactions/Transactions'
import AddressInfo from './AddressInfo/AddressInfo'
import TotalOrder from './TotalOrder/TotalOrder'
import StatusLabel from '../../StatusLabel/StatusLabel'

const OrderInfo = ({ order }) => {
  const { address } = order.shippingData
  return (
    <div>
      <div className="fl w-40-ns pv3 pl0">
        <time className="c-on-base">
          <FormattedDate
            value={order.creationDate}
            year={settings.year}
            month={settings.month}
            day={settings.day}
          ></FormattedDate>
          <StatusLabel status={order.status}></StatusLabel>
        </time>
      </div>
      <div className="w-100 fl w-60-ns pv3-ns pr0">
        <RepeatOrder order={order}></RepeatOrder>
      </div>
      <section className="w-100 fl mt5 mb2-l mb2-xl">
        <AddressInfo address={address}></AddressInfo>
        <Transactions order={order}></Transactions>
        <TotalOrder order={order}></TotalOrder>
      </section>
    </div>
  )
}

export default OrderInfo
