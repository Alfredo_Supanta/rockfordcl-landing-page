import React from 'react'

const AddressInfo = ({ address }) => {
  return (
    <article className="w-100 fl w-third-m pr3-m mb5">
      <section className="pa5 ba bw1 b--muted-5 h4-plus overflow-y-scroll bg-base">
        <h3 className="c-on-base mt2 mb5 tracked-mega lh-solid ttu f6">
          Dirección
        </h3>
        <div className="lh-copy f6">
          <strong>{address.receiverName}</strong>
          <br />
          <div className="address-summary address-summary-CHL">
            <span>
              <span className="street">{address.street}</span>
            </span>
            <span>
              <span className="number-delimiter"></span>
              <span className="number">{address.number}</span>
            </span>
            <br className="line1-delimiter" />
            <span>
              <span className="neighborhood">{address.neighborhood}</span>
              <span className="neighborhood-delimiter-after">,</span>
            </span>
            <span>
              <span className="state">{address.state}</span>
            </span>
            <br className="line2-delimiter" />
            <span className="country">{address.country}</span>
          </div>
        </div>
      </section>
    </article>
  )
}

export default AddressInfo
