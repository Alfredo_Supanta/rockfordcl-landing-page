import React, { useState } from 'react'
import { FormattedNumber } from 'react-intl'

import ToggleInfoIcon from './ToggleInfoIcon'

const Payment = ({ payment, order }) => {
  const hiddenClass =
    'o-0 max-h-0 pl0 ma0 transition-opacity-06s transition-max-height-06s list c-muted-1 overflow-y-hidden'
  const displayedClass =
    'o-100 max-h-999 pl0 ma0 transition-opacity-06s transition-max-height-06s list c-muted-1 overflow-y-hidden'

  const [shouldHideInfo, setShouldHideInfo] = useState(true)

  const renderConnectorInfo = (info) => {
    const elements = []
    for (const infoTitle in info) {
      if (Object.hasOwnProperty.call(info, infoTitle)) {
        const data = info[infoTitle]
        if (data !== null) {
          elements.push(
            <li className=" f7">
              <span className="mr3 fw5">{infoTitle}:</span>
              <span>{data}</span>
            </li>
          )
        }
      }
    }
    return elements
  }

  return (
    <div className="mb5">
      <div className="dib ma0 pa0 f6 lh-copy">
        {payment.group}&nbsp;
        <div className="dib">
          <FormattedNumber
            currency={order.storePreferencesData.currencyCode}
            style="currency"
            value={payment.value / 100}
          />
          <span className="fw5"> ({payment.installments}x)</span>
        </div>
        <section>
          <button
            type="button"
            className=" dt mv3 pl0 bw0 bg-transparent input-reset c-muted-1 lh-solid f7 fw6"
            onClick={() => setShouldHideInfo((prevState) => !prevState)}
          >
            <ToggleInfoIcon shouldHideInfo={shouldHideInfo}></ToggleInfoIcon>
            <span className="dtr vtex-my-orders-app-3-x-font myo-font">
              Informaciones adicionales
            </span>
          </button>

          <ul className={shouldHideInfo ? hiddenClass : displayedClass}>
            {renderConnectorInfo(payment.connectorResponses)}
          </ul>
        </section>
      </div>
    </div>
  )
}

export default Payment
