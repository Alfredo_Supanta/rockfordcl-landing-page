import React from 'react'

const ToggleInfoIcon = ({ shouldHideInfo }) => {
  if (shouldHideInfo) {
    return (
      <svg
        className="dtr v-mid mr2 h-15px w-15px"
        viewBox="0 0 283 283"
        xmlns="http://www.w3.org/2000/svg"
      >
        <g fill="currentColor" fillRule="evenodd">
          <circle
            className="c-muted-1"
            cx="141.5"
            cy="141.5"
            r="141.5"
          ></circle>
          <path
            fill="#FFF"
            d="M123.98 209.078v-50.58H73V123.58h50.98V73h33.986v50.58h51.112v34.917h-51.112v50.58"
          ></path>
        </g>
      </svg>
    )
  }
  return (
    <svg
      className="dtr v-mid mr2 h-15px w-15px"
      viewBox="0 0 283 283"
      xmlns="http://www.w3.org/2000/svg"
    >
      <g fill="currentColor" fillRule="evenodd">
        <circle className="c-muted-1" cx="141.5" cy="141.5" r="141.5"></circle>
        <path fill="#FFF" d="M73 158.497V123.58h136.078v34.917"></path>
      </g>
    </svg>
  )
}

export default ToggleInfoIcon
