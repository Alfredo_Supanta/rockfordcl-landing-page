import React from 'react'

import Payment from './Payment/Payment'

const Transaction = ({ transaction, order }) => {
  return (
    <>
      {transaction.payments.map((payment) => (
        <Payment payment={payment} order={order} key={payment.id}></Payment>
      ))}
    </>
  )
}

export default Transaction
