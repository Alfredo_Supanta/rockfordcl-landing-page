import React from 'react'

import Transaction from './Transaction/Transaction'

const Transactions = ({ order }) => {
  const { transactions } = order.paymentData
  return (
    <article className="w-100 fl w-third-m pr3-m mb5">
      <section className="pa5 ba bw1 b--muted-5 overflow-y-scroll bg-base h4-plus">
        <h3 className="c-on-base mt2 mb5 tracked-mega lh-solid ttu f6">
          Forma de Pago
        </h3>
        {transactions.map((transaction) => (
          <Transaction
            transaction={transaction}
            order={order}
            key={transaction.transactionId}
          ></Transaction>
        ))}
      </section>
    </article>
  )
}

export default Transactions
