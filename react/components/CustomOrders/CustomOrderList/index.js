import React, { useEffect, useState } from 'react'
import { ContentWrapper } from 'vtex.my-account-commons'

import CollapsedOrdersList from './CollapsedOrders/CollapsedOrderList'
import OrdersList from './Orders/Orders'
import { getFullDataOfOrders, getOrders } from '../Utils/Service'
import LoadingIcon from '../LoadingIcon/LoadingIcon'
import '../CustomOrder.global.css'

const numberOfUncollapsedOrders = 3

const CustomOrders = () => {
  const [orderState, setOrderState] = useState({
    orders: [],
    collapsedOrders: [],
    loading: false,
  })

  const [isLoading, setIsLoading] = useState(false)

  useEffect(() => {
    const fetchOrders = async () => {
      // Get order list and splice to only retrieve the full data of the first positions
      setIsLoading(true)
      const data = await getOrders()
      const collapsedOrders =
        data.list.length > numberOfUncollapsedOrders ? data.list : []
      const ordersData =
        data.list.length > numberOfUncollapsedOrders
          ? collapsedOrders.splice(0, numberOfUncollapsedOrders)
          : data.list
      const orders = await getFullDataOfOrders(ordersData)
      setIsLoading(false)
      setOrderState({
        collapsedOrders,
        orders,
      })
    }

    fetchOrders()
  }, [])

  return (
    <ContentWrapper titleId="Pedidos" namespace="pedidos">
      {() => (
        <>
          {isLoading && <LoadingIcon></LoadingIcon>}
          {orderState.orders.length === 0 && !isLoading && (
            <p>No se han realizado ordenes </p>
          )}
          <OrdersList orders={orderState.orders}></OrdersList>
          <CollapsedOrdersList
            collapsedOrders={orderState.collapsedOrders}
          ></CollapsedOrdersList>
        </>
      )}
    </ContentWrapper>
  )
}

export default CustomOrders
