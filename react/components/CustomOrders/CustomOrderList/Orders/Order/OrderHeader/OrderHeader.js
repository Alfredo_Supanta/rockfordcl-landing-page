import React from 'react'
import { FormattedDate, FormattedNumber } from 'react-intl'

import StatusLabel from '../../../../StatusLabel/StatusLabel'
import settings from '../../../../Utils/DateSettings'

const OrderHeader = ({ order }) => {
  return (
    <div className="cf w-100 pa5 ph7-ns bb b--muted-4 bg-muted-5 lh-copy o-100">
      <div className="fl db w-25-ns w-50-s">
        <div className="w-100 f7 f6-xl fw4 c-muted-1 ttu">Fecha del pedido</div>
        <div className="db pv0 f6 fw5 c-on-base f5-l">
          <FormattedDate
            value={order.creationDate}
            year={settings.year}
            month={settings.month}
            day={settings.day}
          ></FormattedDate>
        </div>
      </div>
      <div className="fr fl-ns w-50">
        <div className="db w-100 f7 f6-xl fw4 c-muted-1 ttu tr tl-ns">
          Total
        </div>
        <div className=" db w-100 f6 fw5 c-muted-1 tr tl-ns f5-l">
          <FormattedNumber
            currency={order.storePreferencesData.currencyCode}
            style="currency"
            value={order.value / 100}
          />
          <div className="absolute dn dib-ns ml3 mt2"></div>
        </div>
      </div>
      <div className="fl mt3 mt0-ns w-25-ns w-100-s">
        <div className="mb3 mb0-xl tl tr-ns f7 f6-xl fw4 c-muted-1">
          # {order.orderId}
        </div>
        <div className="dn mb3 mb0-xl tl tr-ns f7 f6-xl fw4 c-muted-1">
          # 00-{order.orderId}
        </div>
        <div className="tr-ns mt2-ns">
          <StatusLabel status={order.status}></StatusLabel>
        </div>
      </div>
    </div>
  )
}

export default OrderHeader
