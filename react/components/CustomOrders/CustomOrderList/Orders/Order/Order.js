import React from 'react'

import OrderBody from './OrderBody/OrderBody'
import OrderHeader from './OrderHeader/OrderHeader'

const Order = ({ order }) => {
  return (
    <article className="w-100 mv7 ba overflow-hidden b--muted-4">
      <OrderHeader order={order}> </OrderHeader>
      <OrderBody order={order}> </OrderBody>
    </article>
  )
}

export default Order
