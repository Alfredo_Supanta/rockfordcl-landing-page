import React from 'react'

import FollowOrder from '../../../../Buttons/FollowOrder/FollowOrder'
import OrderDetails from '../../../../Buttons/OrderDetails'
import RepeatOrder from '../../../../Buttons/RepeatOrder/RepeatOrder'
import ReturnOrder from '../../../../Buttons/ReturnOrder/ReturnOrder'
import ProductList from './Products/ProductList'

const OrderBody = ({ order }) => {
  return (
    <>
      <div className="cf pa5 pa6-l bg-base bt-0">
        <div className="fl w-100 w-70-ns">
          <ProductList order={order}></ProductList>
        </div>
        <div className="cf fr db w-100 w-30-ns pt0-xl pt5">
          <RepeatOrder order={order}></RepeatOrder>
          <OrderDetails order={order}></OrderDetails>
          <ReturnOrder order={order}></ReturnOrder>
          <FollowOrder order={order}></FollowOrder>
        </div>
      </div>
    </>
  )
}

export default OrderBody
