import React from 'react'
import { FormattedDate, FormattedNumber } from 'react-intl'

import GetTalla from '../../../../../../Utils/GetTallaHook'

const Product = ({ product, logisticInfo, order }) => {
  const talla = GetTalla(product)

  return (
    <div className="w-100 pb2 pt2 overflow-y-hidden">
      <div className="v-top dib w-20 h-auto">
        <img
          src={product.imageUrl}
          alt={product.name}
          crossOrigin="anonymous"
        />
      </div>
      <div className="dib w-80 pl3 c-on-base f6 fw4 lh-copy">
        <h4 className="db mb1 mt0">
          <a
            target="_blank"
            href={product.detailUrl}
            className="c-link hover-c-link c-link--visited fw4 f6 f5-l link"
            rel="noopener noreferrer"
          >
            {product.name}
          </a>
        </h4>
        {talla && <p className="db mt1 f6">Talla: {talla}</p>}

        <p className="db mt1 f6">
          Entregue hasta el{' '}
          <FormattedDate
            value={logisticInfo.shippingEstimateDate}
          ></FormattedDate>
        </p>
        <span className="db mt0 mb2 f6 fw6">
          <span className="pr3">
            {product.quantity} {product.measurementUnit}
          </span>
          <span>
            <FormattedNumber
              currency={order.storePreferencesData.currencyCode}
              style="currency"
              value={(product.sellingPrice * product.unitMultiplier) / 100}
            />
          </span>
        </span>
      </div>
    </div>
  )
}

export default Product
