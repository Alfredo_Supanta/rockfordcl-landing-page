import React from 'react'

import Product from './Product/Product'

const ProductList = ({ order }) => {
  const { items } = order
  const logisticsInfoList = order.shippingData.logisticsInfo
  return items.map((product, index) => {
    product.skuName = order.itemMetadata.Items[index].SkuName
    return (
      <Product
        product={product}
        logisticInfo={logisticsInfoList[index]}
        order={order}
        key={product.productId}
      ></Product>
    )
  })
}

export default ProductList
