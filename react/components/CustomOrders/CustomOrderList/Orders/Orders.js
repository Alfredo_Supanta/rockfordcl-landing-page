import React from 'react'

import Order from './Order/Order'

const OrdersList = ({ orders }) => {
  return orders.map((order) => (
    <Order order={order} key={order.orderId}></Order>
  ))
}

export default OrdersList
