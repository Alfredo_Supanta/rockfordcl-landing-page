import React from 'react'

import CollapsedOrder from './CollapsedOrder/CollapsedOrder'

const CollapsedOrdersList = ({ collapsedOrders }) => {
  return collapsedOrders.map((order) => (
    <CollapsedOrder order={order} key={order.orderId}></CollapsedOrder>
  ))
}

export default CollapsedOrdersList
