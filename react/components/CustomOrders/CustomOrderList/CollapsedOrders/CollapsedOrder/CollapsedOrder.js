import React from 'react'
import { FormattedDate } from 'react-intl'

import StatusLabel from '../../../StatusLabel/StatusLabel'
import settings from '../../../Utils/DateSettings'

const CollapsedOrder = ({ order }) => {
  return (
    <a
      className="no-underline db cf w-100 pa5 ph7-ns ba b--muted-4 bg-muted-5 mb3 pointer grow o-100"
      href={`#/pedidos/${order.orderId}`}
    >
      <div className="fl-ns w-50-ns">
        <div className="w-100 f7 f6-xl fw4 c-muted-1 ttu">Fecha del pedido</div>
        <div className="db db-xl pv0 f6 fw5 c-on-base f5-l">
          <FormattedDate
            value={order.creationDate}
            year={settings.year}
            month={settings.month}
            day={settings.day}
          ></FormattedDate>
        </div>
      </div>
      <div className="fl-ns mt3 mt0-ns w-50-ns">
        <div className="mb3 mb0-xl tl tr-ns f7 f6-xl fw4 c-muted-1">
          # {order.orderId}{' '}
        </div>
        <div className="tr-ns mt2-ns">
          <StatusLabel status={order.status}></StatusLabel>
        </div>
      </div>
    </a>
  )
}

export default CollapsedOrder
