import React from 'react'

const StatusLabel = ({ status }) => {
  if (status === 'invoiced') {
    return (
      <div className="dib br2 pv2 ph3 f7 fw5 tc bg-success">
        <span className="c-on-success">Enviando el pedido</span>
      </div>
    )
  }
  if (status === 'canceled') {
    return (
      <div className="dib br2 pv2 ph3 f7 fw5 tc bg-dark-gray">
        <span className="c-on-base--inverted">Cancelado</span>
      </div>
    )
  }
  return null
}

export default StatusLabel
