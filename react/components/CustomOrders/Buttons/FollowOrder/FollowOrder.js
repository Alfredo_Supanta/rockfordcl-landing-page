import React, { useState, useEffect } from 'react'
import { Link } from 'vtex.render-runtime'

import './FollowOrder.global.css'

const FollowOrder = ({ order }) => {
  const [link, setLink] = useState()

  useEffect(() => {
    const fetchLink = async () => {
      const responses = await fetch('/api/sessions', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: '{"public":{"country":{"value":"."},"postalCode":{"value":"."}}}',
      })

      const data = await responses.json()

      setLink(data)
    }

    fetchLink()
  }, [])
  // console.log(link)
  const vtexToken = link ? link.sessionToken : ''

  return (
    <Link
      className="b--action-primary bg-action-primary .c-on-action-primary .t-action btn-seguir-envio"
      to={`https://www.siguetucompra.cl/compra/${order.orderId}?vtextoken=${vtexToken}`}
      target="_blank"
      rel="noopener noreferrer"
    >
      Sigue tu compra
    </Link>
  )
}

export default FollowOrder
