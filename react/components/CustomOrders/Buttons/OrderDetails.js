import React from 'react'

const OrderDetails = ({ order }) => {
  return (
    <a
      className="db tc pv3 ph5 br2 w-100 f6 fw4 link bg-action-secondary c-on-action-secondary hover-action-secondary mt5"
      href={`#/pedidos/${order.orderId}`}
    >
      <span className="db pv2">Ver detalles del pedido</span>
    </a>
  )
}

export default OrderDetails
