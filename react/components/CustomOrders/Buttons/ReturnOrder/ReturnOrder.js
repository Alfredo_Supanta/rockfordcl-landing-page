import React, { useState, useEffect } from 'react'
import { Link } from 'vtex.render-runtime'

import daysDiff from '../../Utils/DaysDiff'
import './ReturnOrder.global.css'

const ReturnOrder = ({ order }) => {
  const [link, setLink] = useState()

  useEffect(() => {
    const fetchLink = async () => {
      const response = await fetch('/api/sessions', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: '{"public":{"country":{"value":"."},"postalCode":{"value":"."}}}',
      })

      const data = await response.json()

      setLink(data)
    }

    fetchLink()
  }, [])
  // console.log(link)
  const vtexToken = link ? link.sessionToken : ''

  const today = new Date(Date.now())
  const orderDate = new Date(order.creationDate)
  const shouldRender =
    daysDiff(orderDate, today) < 120 && order.status !== 'canceled'

  return (
    <>
      {shouldRender && (
        <Link
          className="vtex-btn_devolucion"
          to={`https://www.siguetucompra.cl/compra/${order.orderId}?vtextoken=${vtexToken}`}
          target="_blank"
          rel="noopener noreferrer"
        >
          Hacer un cambio o devolución
        </Link>
      )}
    </>
  )
}

export default ReturnOrder
