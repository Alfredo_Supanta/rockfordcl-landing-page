import React from 'react'
import './RepeatOrder.global.css'
import { useRuntime } from 'vtex.render-runtime'

const RepeatOrder = ({ order }) => {
  const { navigate } = useRuntime()
  const repeatOrderHandler = () => {
    const { items } = order
    let url = '/checkout/cart/add/?'
    items.forEach((product) => {
      url += `sku=${product.id}&qty=${product.quantity}&seller=${product.seller}&sc=1&`
    })
    navigate({
      to: url,
      replace: true,
    })
    // we use navigate to avoid XSS attacks and we need to reload the page in order to add the items to the cart
    setTimeout(function () {
      window.location.reload()
    }, 500)
  }

  return (
    <button
      onClick={repeatOrderHandler}
      className="pointer c-link hover-c-link pa0 bg-transparent bn cf db link tl w5 f6 fw4 flex items-center mb5 repeat-button-left"
    >
      <svg
        className=""
        fill="#134CD8"
        version="1.1"
        xmlns="http://www.w3.org/2000/svg"
        x="0px"
        y="0px"
        viewBox="0 0 16 16"
        width="16"
        height="16"
      >
        <title>Hacer pedido de nuevo</title>
        <g>
          <circle cx="3.5" cy="12.5" r="2.5"></circle>
          <path d="M8,1C6.127,1,4.35,1.758,3.052,3.052L0.9,0.9L0.2,7.3l6.4-0.7L4.465,4.465C5.393,3.542,6.662,3,8,3 c2.757,0,5,2.243,5,5s-2.243,5-5,5v2c3.859,0,7-3.14,7-7S11.859,1,8,1z"></path>
        </g>
      </svg>
      <span className="ml3 no-underline">Hacer pedido de nuevo</span>
    </button>
  )
}

export default RepeatOrder
