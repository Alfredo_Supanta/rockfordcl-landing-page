import React, { Fragment, useState, useEffect } from 'react'
import './CorebizLoaderComponent.global.css'

const CorebizLoaderComponent = () => {
  const [cargado, cargando] = useState(false)

  useEffect(() => {
    setTimeout(() => {
      cargando(true)
    }, 3500)
  })

  return (
    <Fragment>
      {!cargado ? (
        <div className="loader-box">
          <img
            className="loader-img"
            src="https://rockfordcl.vtexassets.com/arquivos/logo.png?v=1"
          />
        </div>
      ) : (
        (document.querySelector(
          '.vtex-flex-layout-0-x-flexCol--loader-col'
        ).style.display = 'none')
      )}
    </Fragment>
  )
}

export default CorebizLoaderComponent
