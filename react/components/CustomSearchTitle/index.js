import React, { useEffect, useState, useRef } from 'react'
import { useSearchPage } from 'vtex.search-page-context/SearchPageContext'
import './CustomSearchTitle.global.css'

const CustomSearchTitle = () => {
  const { searchQuery } = useSearchPage()
  const [breadcrumbLength, setBreadcrumbLength] = useState(1)
  const collectionTitle = useRef(null)

  useEffect(() => {
    if (searchQuery.data.facets.breadcrumb) {
      setBreadcrumbLength(searchQuery.data.facets.breadcrumb.length)
    }
  }, [searchQuery.data.facets.breadcrumb])

  return (
    <>
      {breadcrumbLength === 0 && (
        <h3 className="custom-search-title" ref={collectionTitle}>
          {searchQuery.data.searchMetadata.titleTag}
        </h3>
      )}
    </>
  )
}

export default CustomSearchTitle
