import React, { useEffect } from 'react'
// import { useProduct } from 'vtex.product-context';

const LianasUrbanSafari = () => {
  // const itemProduct = useProduct();
  useEffect(() => {
    // pathname
    const pathnameCompleto = window.location.pathname
    // console.log(pathnameCompleto, "=> pathnameCompleta")

    if (pathnameCompleto.includes('/484')) {
      const contLianas = document.querySelector(
        '.vtex-slider-layout-0-x-sliderLayoutContainer--lianas'
      )

      const lianas = document.querySelectorAll(
        '.vtex-slider-layout-0-x-sliderTrack--lianas'
      )

      contLianas.style.visibility = 'visible'

      const liana1 = document.querySelectorAll(
        '.vtex-slider-layout-0-x-sliderTrack--lianas'
      )[0].childNodes[2].childNodes[0]
      const liana2 = document.querySelectorAll(
        '.vtex-slider-layout-0-x-sliderTrack--lianas'
      )[0].childNodes[3].childNodes[0]
      liana1.classList.add('liana-1')
      liana2.classList.add('liana-2')

      const liana12 = document.querySelectorAll(
        '.vtex-slider-layout-0-x-sliderTrack--lianas'
      )[0].childNodes[2]
      const liana11 = document.querySelectorAll(
        '.vtex-slider-layout-0-x-sliderTrack--lianas'
      )[0].childNodes[3]
      liana11.classList.add('liana-11')
      liana12.classList.add('liana-12')

      document
        .querySelectorAll('.vtex-slider-layout-0-x-sliderTrack--lianas')[0]
        .childNodes[0].classList.add('hidden')
      document
        .querySelectorAll('.vtex-slider-layout-0-x-sliderTrack--lianas')[0]
        .childNodes[1].classList.add('hidden')
      document
        .querySelectorAll('.vtex-slider-layout-0-x-sliderTrack--lianas')[0]
        .childNodes[4].classList.add('hidden')
      document
        .querySelectorAll('.vtex-slider-layout-0-x-sliderTrack--lianas')[0]
        .childNodes[5].classList.add('hidden')

      console.log(lianas, 'lianas')
    }
    /* else{
            const lianas = document.querySelectorAll(".vtex-slider-layout-0-x-sliderTrack--lianas")
            lianas.forEach(liana => {
                liana.classList.add("hidden");
            });
            console.log("no es urban safari")
        } */
  }, [])

  return null
}

export default LianasUrbanSafari
