import React, { useState, useEffect, Fragment } from 'react'
import './BusquedaTallas.global.css'

const BusquedaTallas = () => {
  const [categoria, guardarCategoria] = useState('')
  const [genero, guardarGenero] = useState('')
  const [tallas, guardarTallas] = useState(['SI'])
  const [validar, setValidar] = useState(false)

  // Generar URL de búsqueda
  const conteoTallas = (sizes) => {
    let conteoSizes = ''

    for (let i = 0; i < sizes.length; i++) {
      if (sizes[i] !== 'SI') {
        conteoSizes += ',talla'
      } else {
        conteoSizes += ',promotion'
      }
    }

    return conteoSizes
  }

  const conteoTallasZapatos = (sizes) => {
    let conteoSizes = ''

    for (let i = 0; i < sizes.length; i++) {
      if (sizes[i] !== 'SI') {
        conteoSizes += ',talla,talla'
      } else {
        conteoSizes += ',promotion'
      }
    }

    return conteoSizes
  }

  let newUrl = ''

  tallas.sort((tallaA, tallaB) =>
    tallaA.localeCompare(tallaB, undefined, {
      numeric: true,
      sensitivity: 'base',
    })
  )

  if (categoria === 'Vestuario') {
    if (newUrl) {
      newUrl = `/${genero}/${categoria}/${tallas
        .join('/')
        .toLowerCase()}/?initialMap=c,c&initialQuery=${genero}/${categoria}&map=category-1,category-2${conteoTallas(
        tallas
      )}`
    } else {
      newUrl = `/${genero}/${categoria}/${tallas
        .join('/')
        .toLowerCase()}/?initialMap=c,c&initialQuery=${genero}/${categoria}&map=category-1,category-2${conteoTallas(
        tallas
      )}`
    }
  } else if (newUrl) {
    newUrl = `/${genero}/${categoria}/${tallas
      .join('/')
      .toLowerCase()}/?initialMap=c,c&initialQuery=${genero}/${categoria}&map=category-1,category-2${conteoTallasZapatos(
      tallas
    )}`
  } else {
    newUrl = `/${genero}/${categoria}/${tallas
      .join('/')
      .toLowerCase()}/?initialMap=c,c&initialQuery=${genero}/${categoria}&map=category-1,category-2${conteoTallasZapatos(
      tallas
    )}`
  }

  useEffect(() => {
    // Capturar categoría principal (calzado, vestimenta, accesorios)
    const optCategoria = document.querySelector(
      '.vtex-rich-text-0-x-paragraph--modal-tallas-title'
    )

    if (
      optCategoria.classList.contains('vtex-rich-text-0-x-paragraph--calzado')
    ) {
      guardarCategoria('Calzado')
    } else if (
      optCategoria.classList.contains('vtex-rich-text-0-x-paragraph--ropa')
    ) {
      guardarCategoria('Vestuario')
    } else {
      guardarCategoria('Accesorios')
    }

    const linkGenero = document.querySelectorAll(
      '.vtex-menu-2-x-menuContainer--menu-lista-genero .vtex-menu-2-x-styledLinkContainer .vtex-menu-2-x-styledLink'
    )

    linkGenero.forEach((link) => {
      link.addEventListener('click', (e) => {
        e.preventDefault()
      })
    })

    // Toggle de clase activa para género
    const optGenero = document.querySelectorAll(
      '.vtex-menu-2-x-menuContainer--menu-lista-genero .vtex-menu-2-x-styledLinkContainer'
    )

    const sizesMen = document.querySelectorAll(
      '.vtex-menu-2-x-styledLinkContainer--talla-hombre'
    )

    const sizesWoman = document.querySelectorAll(
      '.vtex-menu-2-x-styledLinkContainer--talla-mujer'
    )

    optGenero.forEach((opcion) => {
      opcion.addEventListener('click', (e) => {
        e.stopPropagation()
        for (let i = 0; i < optGenero.length; i++) {
          const element = optGenero[i]

          if (
            element !== e.currentTarget &&
            element.classList.contains('active--genero')
          ) {
            element.classList.remove('active--genero')
          }
        }

        e.currentTarget.classList.toggle('active--genero')

        btnOff(e)
      })
    })

    const btnOff = (e) => {
      if (
        e.currentTarget.firstElementChild.getAttribute('href') === '/hombre'
      ) {
        sizesWoman.forEach((size) => size.classList.toggle('disabled-btn'))
        sizesMen.forEach((size) => {
          if (size.classList.value.includes('disabled-btn')) {
            size.classList.remove('disabled-btn')
          }
        })
      } else {
        sizesMen.forEach((size) => size.classList.toggle('disabled-btn'))
        sizesWoman.forEach((size) => {
          if (size.classList.value.includes('disabled-btn')) {
            size.classList.remove('disabled-btn')
          }
        })
      }
    }

    // Toggle de clase activa para tallas
    const optTallas = document.querySelectorAll(
      '.vtex-menu-2-x-menuContainer--menu-lista-tallas .vtex-menu-2-x-styledLinkContainer'
    )

    const optTallas2 = document.querySelectorAll(
      '.vtex-menu-2-x-menuContainer--menu-lista-tallas-calzado .vtex-menu-2-x-styledLinkContainer'
    )

    optTallas.forEach((opcion) => {
      opcion.addEventListener('click', (e) => {
        e.preventDefault()
        if (optTallas2.length === 0) {
          e.currentTarget.classList.toggle('active--tallas')
        } else {
          for (let i = 0; i < optTallas2.length; i++) {
            const element = optTallas2[i]

            if (element.classList.contains(e.currentTarget.classList[2])) {
              e.currentTarget.classList.toggle('active--tallas')
              element.classList.toggle('active--similar')
            }
          }
        }
      })
    })
  }, [])

  useEffect(() => {
    // Cambiar url del boton
    const btnResultados = document.querySelector(
      '.vtex-rich-text-0-x-link--btn-ver-resultados'
    )

    if (genero !== '' && tallas.length > 0) {
      btnResultados.removeAttribute('disabled')
      btnResultados.setAttribute('href', newUrl)
    } else {
      btnResultados.setAttribute('href', '#')
      btnResultados.setAttribute('disabled', 'disabled')
    }

    // Capturar categoría secundaria (hombre, mujer, niños)
    const optGenero = document.querySelectorAll(
      '.vtex-menu-2-x-menuContainer--menu-lista-genero .vtex-menu-2-x-styledLinkContainer'
    )

    optGenero.forEach((opcion) => {
      opcion.addEventListener('click', (e) => {
        e.preventDefault()
        if (
          genero !=
          e.currentTarget.firstElementChild
            .getAttribute('href')
            .split('/')
            .join('')
        ) {
          guardarGenero(
            e.currentTarget.firstElementChild
              .getAttribute('href')
              .split('/')
              .join('')
          )
        } else {
          guardarGenero('')
        }
      })
    })

    // Capturar tallas
    const optTallas = document.querySelectorAll(
      '.vtex-menu-2-x-menuContainer--menu-lista-tallas .vtex-menu-2-x-styledLinkContainer'
    )

    optTallas.forEach((opcion) => {
      opcion.addEventListener('click', (e) => {
        e.stopPropagation()
        if (
          e.currentTarget.parentElement.parentElement.classList.contains(
            'vtex-menu-2-x-menuContainer--menu-lista-tallas-calzado'
          )
        ) {
          const tallaCalzado =
            e.currentTarget.firstElementChild.getAttribute('href')

          if (!tallas.includes(tallaCalzado)) {
            guardarTallas([...tallas, tallaCalzado])
          } else {
            const nuevasTallas = tallas.filter((talle) => talle != tallaCalzado)

            guardarTallas(nuevasTallas)
          }
        } else {
          const talla =
            e.currentTarget.firstElementChild.firstElementChild.innerHTML

          if (!tallas.includes(talla)) {
            guardarTallas([...tallas, talla])
          } else {
            const nuevasTallas = tallas.filter((talle) => talle != talla)

            guardarTallas(nuevasTallas)
          }
        }
      })
    })

    btnResultados.addEventListener('click', (e) => {
      if (btnResultados.getAttribute('disabled')) {
        e.preventDefault()
        setValidar(true)
      } else {
        window.location.href = newUrl
      }
    })
  }, [tallas, genero, newUrl])

  return (
    <Fragment>
      {' '}
      {!genero && validar ? (
        <div className="msg-error">
          <span className="msg-error_icon" />{' '}
          <span className="msg-error_text"> Debe seleccionar un género </span>{' '}
        </div>
      ) : null}{' '}
      {!tallas.length > 0 && validar ? (
        <div className="msg-error">
          <span className="msg-error_icon" />{' '}
          <span className="msg-error_text">
            Debe seleccionar al menos una talla{' '}
          </span>{' '}
        </div>
      ) : null}{' '}
    </Fragment>
  )
}

export default BusquedaTallas
