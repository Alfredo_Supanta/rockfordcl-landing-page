import React, { useEffect } from 'react'

function RenameFilter() {
  useEffect(() => {
    setTimeout(function () {
      // extraigo de la url el pathname completo
      const pathnameComplete = window.location.pathname
      // console.log(pathnameComplete, "=> pathnameCompleta")

      const urlCompleta = window.location.href
      // console.log(urlCompleta, "=> urlCompleta")

      const pathname1 = pathnameComplete.split('/')[1]
      // console.log(pathname1, "=> pathname1")

      const pathname2 = pathnameComplete.split('/')[2]
      // console.log(pathname2, "=> pathname2")

      const pathname3 = pathnameComplete.split('/')[3]
      // console.log(pathname3, "=> pathname3")

      // ---------------------------------------------------------------------------------------------------------
      // MENU SALE O COLECCION 138
      if (urlCompleta.includes(`138`)) {
        if (pathnameComplete.includes('/si')) {
          document.querySelector(
            `.vtex-search-result-3-x-galleryTitle--layout`
          ).innerHTML = 'Sale'
          // console.log("TRAE DESCUENTO")
        } else if (
          !pathnameComplete.includes('/si') &&
          urlCompleta.includes('138')
        ) {
          document.querySelector(
            `.vtex-search-result-3-x-galleryTitle--layout`
          ).innerHTML = 'Todo Rockford'
          // console.log("NO TRAE DESCUENTO")
        }

        // console.log("COLECCION 138")

        // CUALQUIER OTRA CATEGORIA, COLECCION O BUSQUEDA QUE TENGA APLICADO UN DESCUENTO
      } else if (
        pathnameComplete.includes('/si') ||
        !pathnameComplete.includes('/si')
      ) {
        // CATEGORIA
        // ------------------------------------------------------------------------------------------------
        if (urlCompleta.includes(`&map=category-1`)) {
          // trae 3 pathname
          if (
            urlCompleta.includes(
              `${pathname1}/${pathname2}/${pathname3}&map=category-1`
            )
          ) {
            if (pathname3.includes('-')) {
              // trea todas las palabras entre guiones
              const palabrasSguion = pathname3.substr(0).split('-')

              // si trae 5 palabras
              if (palabrasSguion.length === 5) {
                const primeraPalbra = palabrasSguion[0]
                const primeraLetra1 = primeraPalbra.charAt(0).toUpperCase()
                const restoPalabrax1 = primeraPalbra.slice(1)
                const palabraCompleta1 = primeraLetra1 + restoPalabrax1

                const segundaPalbra = palabrasSguion[1]

                const terceraPalbra = palabrasSguion[2]
                const primeraLetra3 = terceraPalbra.charAt(0).toUpperCase()
                const restoPalabrax3 = terceraPalbra.slice(1)
                const palabraCompleta3 = primeraLetra3 + restoPalabrax3

                const cuartaPalbra = palabrasSguion[3]

                const quintaPalbra = palabrasSguion[4]
                const primeraLetra5 = quintaPalbra.charAt(0).toUpperCase()
                const restoPalabrax5 = quintaPalbra.slice(1)
                const palabraCompleta5 = primeraLetra5 + restoPalabrax5
                const restoPalabrax5Con = palabraCompleta5.replace('n', 'ñ')

                const fraseTotal =
                  palabraCompleta1 +
                  ' ' +
                  segundaPalbra +
                  ' ' +
                  palabraCompleta3 +
                  ' ' +
                  cuartaPalbra +
                  ' ' +
                  restoPalabrax5Con
                document.querySelector(
                  `.vtex-search-result-3-x-galleryTitle--layout`
                ).innerHTML = fraseTotal

                // console.log(fraseTotal, "frase completa 5")

                // si trae 4 palabras
              } else if (palabrasSguion.length === 4) {
                const primeraPalbra = palabrasSguion[0]
                const primeraLetra1 = primeraPalbra.charAt(0).toUpperCase()
                const restoPalabrax1 = primeraPalbra.slice(1)
                const palabraCompleta1 = primeraLetra1 + restoPalabrax1

                const segundaPalbra = palabrasSguion[1]
                const primeraLetra2 = segundaPalbra.charAt(0).toUpperCase()
                const restoPalabrax2 = segundaPalbra.slice(1)
                const palabraCompleta2 = primeraLetra2 + restoPalabrax2

                const terceraPalbra = palabrasSguion[2]

                const cuartaPalbra = palabrasSguion[3]
                const primeraLetra4 = cuartaPalbra.charAt(0).toUpperCase()
                const restoPalabrax4 = cuartaPalbra.slice(1)
                const palabraCompleta4 = primeraLetra4 + restoPalabrax4

                const fraseTotal =
                  palabraCompleta1 +
                  ', ' +
                  palabraCompleta2 +
                  ' ' +
                  terceraPalbra +
                  ' ' +
                  palabraCompleta4
                document.querySelector(
                  `.vtex-search-result-3-x-galleryTitle--layout`
                ).innerHTML = fraseTotal

                // console.log(fraseTotal, "frase completa")

                // si trae 3 palabras
              } else if (palabrasSguion.length === 3) {
                const primeraPalbra = palabrasSguion[0]
                const primeraLetra1 = primeraPalbra.charAt(0).toUpperCase()
                const restoPalabrax1 = primeraPalbra.slice(1)
                const palabraCompleta1 = primeraLetra1 + restoPalabrax1

                const segundaPalbra = palabrasSguion[1]

                const terceraPalbra = palabrasSguion[2]
                const primeraLetra3 = terceraPalbra.charAt(0).toUpperCase()
                const restoPalabrax3 = terceraPalbra.slice(1)
                const palabraCompleta3 = primeraLetra3 + restoPalabrax3

                const fraseTotal =
                  palabraCompleta1 +
                  ' ' +
                  segundaPalbra +
                  ' ' +
                  palabraCompleta3
                document.querySelector(
                  `.vtex-search-result-3-x-galleryTitle--layout`
                ).innerHTML = fraseTotal

                // console.log(fraseTotal, "frase completa")

                // si trae 2 palabras
              } else if (palabrasSguion.length === 2) {
                const primeraPalbra = palabrasSguion[0]
                const primeraLetra1 = primeraPalbra.charAt(0).toUpperCase()
                const restoPalabrax1 = primeraPalbra.slice(1)
                const palabraCompleta1 = primeraLetra1 + restoPalabrax1
                // console.log(palabraCompleta1, "primeraPalbra")

                const segundaPalbra = palabrasSguion[1]
                const primeraLetra2 = segundaPalbra.charAt(0).toUpperCase()
                const restoPalabrax2 = segundaPalbra.slice(1)
                const palabraCompleta2 = primeraLetra2 + restoPalabrax2

                const fraseTotal = palabraCompleta1 + ' ' + palabraCompleta2
                document.querySelector(
                  `.vtex-search-result-3-x-galleryTitle--layout`
                ).innerHTML = fraseTotal

                // console.log(fraseTotal, "segundaPalbra")

                // si trae 1 palabras
              } else if (palabrasSguion.length === 1) {
                const primeraPalbra = palabrasSguion[0]
                const primeraLetra1 = primeraPalbra.charAt(0).toUpperCase()
                const restoPalabrax1 = primeraPalbra.slice(1)

                const fraseTotal = primeraLetra1 + restoPalabrax1
                document.querySelector(
                  `.vtex-search-result-3-x-galleryTitle--layout`
                ).innerHTML = fraseTotal

                // console.log(fraseTotal, "primeraPalbra")
              }
            } else {
              const primeraLtraMayus = pathname3.charAt(0).toUpperCase()
              const restoPalabra = pathname3.slice(1)
              const palabraConLetraMayuscula = primeraLtraMayus + restoPalabra
              document.querySelector(
                `.vtex-search-result-3-x-galleryTitle--layout`
              ).innerHTML = palabraConLetraMayuscula
            }

            // console.log("3 pathname")

            // trae 2 pathname
          } else if (
            urlCompleta.includes(`${pathname1}/${pathname2}&map=category-1`)
          ) {
            if (pathname2.includes('-')) {
              // trea todas las palabras entre guiones
              const palabrasSguion = pathname2.substr(0).split('-')

              // si trae 3 palabras
              if (palabrasSguion.length === 3) {
                const primeraPalbra = palabrasSguion[0]
                const primeraLetra1 = primeraPalbra.charAt(0).toUpperCase()
                const restoPalabrax1 = primeraPalbra.slice(1)
                const palabraCompleta1 = primeraLetra1 + restoPalabrax1

                const segundaPalbra = palabrasSguion[1]

                const terceraPalbra = palabrasSguion[2]
                const primeraLetra3 = terceraPalbra.charAt(0).toUpperCase()
                const restoPalabrax3 = terceraPalbra.slice(1)
                const palabraCompleta3 = primeraLetra3 + restoPalabrax3

                const fraseTotal =
                  palabraCompleta1 +
                  ' ' +
                  segundaPalbra +
                  ' ' +
                  palabraCompleta3
                document.querySelector(
                  `.vtex-search-result-3-x-galleryTitle--layout`
                ).innerHTML = fraseTotal

                // console.log(fraseTotal, "frase completa")

                // si trae 2 palabras
              } else if (palabrasSguion.length === 2) {
                const primeraPalbra = palabrasSguion[0]
                const primeraLetra1 = primeraPalbra.charAt(0).toUpperCase()
                const restoPalabrax1 = primeraPalbra.slice(1)
                const palabraCompleta1 = primeraLetra1 + restoPalabrax1
                // console.log(palabraCompleta1, "primeraPalbra")

                const segundaPalbra = palabrasSguion[1]
                const primeraLetra2 = segundaPalbra.charAt(0).toUpperCase()
                const restoPalabrax2 = segundaPalbra.slice(1)
                const palabraCompleta2 = primeraLetra2 + restoPalabrax2

                const fraseTotal = palabraCompleta1 + ' ' + palabraCompleta2
                document.querySelector(
                  `.vtex-search-result-3-x-galleryTitle--layout`
                ).innerHTML = fraseTotal

                // console.log(fraseTotal, "segundaPalbra")

                // si trae 1 palabras
              } else if (palabrasSguion.length === 1) {
                const primeraPalbra = palabrasSguion[0]
                const primeraLetra1 = primeraPalbra.charAt(0).toUpperCase()
                const restoPalabrax1 = primeraPalbra.slice(1)

                const fraseTotal = primeraLetra1 + restoPalabrax1
                document.querySelector(
                  `.vtex-search-result-3-x-galleryTitle--layout`
                ).innerHTML = fraseTotal

                // console.log(fraseTotal, "primeraPalbra")
              }
            } else {
              const primeraLtraMayus = pathname2.charAt(0).toUpperCase()
              const restoPalabra = pathname2.slice(1)
              const palabraConLetraMayuscula = primeraLtraMayus + restoPalabra
              document.querySelector(
                `.vtex-search-result-3-x-galleryTitle--layout`
              ).innerHTML = palabraConLetraMayuscula
            }

            // trae 1 pathname
          } else if (urlCompleta.includes(`${pathname1}&map=category-1`)) {
            const primeraLtraMayus = pathname1.charAt(0).toUpperCase()
            const restoPalabra = pathname1.slice(1)
            const palabraConLetraMayuscula = primeraLtraMayus + restoPalabra
            document.querySelector(
              `.vtex-search-result-3-x-galleryTitle--layout`
            ).innerHTML = palabraConLetraMayuscula
          }

          // console.log("CATEGORIAA")

          // COLECCIONES
          // ------------------------------------------------------------------------------------------------
        } else if (urlCompleta.includes(`productClusterIds`)) {
          if (urlCompleta.includes(383)) {
            document.querySelector(
              `.vtex-search-result-3-x-galleryTitle--layout`
            ).innerHTML = 'GREEN HOUSE'
          } else if (urlCompleta.includes(335)) {
            document.querySelector(
              `.vtex-search-result-3-x-galleryTitle--layout`
            ).innerHTML = 'Algodón Orgánico'
          } else if (urlCompleta.includes(384)) {
            document.querySelector(
              `.vtex-search-result-3-x-galleryTitle--layout`
            ).innerHTML = 'Lino Orgánico'
          } else if (urlCompleta.includes(355)) {
            document.querySelector(
              `.vtex-search-result-3-x-galleryTitle--layout`
            ).innerHTML = 'Seda'
          } else if (urlCompleta.includes(357)) {
            document.querySelector(
              `.vtex-search-result-3-x-galleryTitle--layout`
            ).innerHTML = 'Cashmere'
          } else if (urlCompleta.includes(358)) {
            document.querySelector(
              `.vtex-search-result-3-x-galleryTitle--layout`
            ).innerHTML = 'Lana Merinoo'
          } else if (urlCompleta.includes(362)) {
            document.querySelector(
              `.vtex-search-result-3-x-galleryTitle--layout`
            ).innerHTML = 'bamboo'
          } else if (urlCompleta.includes(395)) {
            document.querySelector(
              `.vtex-search-result-3-x-galleryTitle--layout`
            ).innerHTML = 'Fibras Recicladas'
          } else if (urlCompleta.includes(407)) {
            document.querySelector(
              `.vtex-search-result-3-x-galleryTitle--layout`
            ).innerHTML = 'Thermore'
          } else if (urlCompleta.includes(397)) {
            document.querySelector(
              `.vtex-search-result-3-x-galleryTitle--layout`
            ).innerHTML = 'Functionality'
          } else if (urlCompleta.includes(345)) {
            document.querySelector(
              `.vtex-search-result-3-x-galleryTitle--layout`
            ).innerHTML = 'Natural Flex'
          } else if (urlCompleta.includes(398)) {
            document.querySelector(
              `.vtex-search-result-3-x-galleryTitle--layout`
            ).innerHTML = 'Wrinkle Free'
          } else if (urlCompleta.includes(401)) {
            document.querySelector(
              `.vtex-search-result-3-x-galleryTitle--layout`
            ).innerHTML = 'WOR'
          } else if (urlCompleta.includes(400)) {
            document.querySelector(
              `.vtex-search-result-3-x-galleryTitle--layout`
            ).innerHTML = 'Travel 360°'
          } else if (urlCompleta.includes(405)) {
            document.querySelector(
              `.vtex-search-result-3-x-galleryTitle--layout`
            ).innerHTML = 'WAX'
          } else if (urlCompleta.includes(404)) {
            document.querySelector(
              `.vtex-search-result-3-x-galleryTitle--layout`
            ).innerHTML = 'Reactive'
          } else if (urlCompleta.includes(289)) {
            document.querySelector(
              `.vtex-search-result-3-x-galleryTitle--layout`
            ).innerHTML = 'Camisas UPF'
          } else if (urlCompleta.includes(402)) {
            document.querySelector(
              `.vtex-search-result-3-x-galleryTitle--layout`
            ).innerHTML = 'Honey Foam'
          } else if (urlCompleta.includes(403)) {
            document.querySelector(
              `.vtex-search-result-3-x-galleryTitle--layout`
            ).innerHTML = 'Soft Stones'
          } else if (urlCompleta.includes(406)) {
            document.querySelector(
              `.vtex-search-result-3-x-galleryTitle--layout`
            ).innerHTML = 'RFID'
          } else if (urlCompleta.includes(363)) {
            document.querySelector(
              `.vtex-search-result-3-x-galleryTitle--layout`
            ).innerHTML = 'Hand Crafted'
          } else if (urlCompleta.includes(369)) {
            document.querySelector(
              `.vtex-search-result-3-x-galleryTitle--layout`
            ).innerHTML = 'Crochet'
          } else if (urlCompleta.includes(364)) {
            document.querySelector(
              `.vtex-search-result-3-x-galleryTitle--layout`
            ).innerHTML = 'Crochet'
          } else if (urlCompleta.includes(368)) {
            document.querySelector(
              `.vtex-search-result-3-x-galleryTitle--layout`
            ).innerHTML = 'Hecho en Chile'
          } else if (urlCompleta.includes(123)) {
            document.querySelector(
              `.vtex-search-result-3-x-galleryTitle--layout`
            ).innerHTML = 'Chaquetas y Parkas'
          } else if (urlCompleta.includes(356)) {
            document.querySelector(
              `.vtex-search-result-3-x-galleryTitle--layout`
            ).innerHTML = 'Todo en Lana'
          } else if (urlCompleta.includes(436)) {
            document.querySelector(
              `.vtex-search-result-3-x-galleryTitle--layout`
            ).innerHTML = 'Chiporro'
          } else if (urlCompleta.includes(423)) {
            document.querySelector(
              `.vtex-search-result-3-x-galleryTitle--layout`
            ).innerHTML = 'Pantuflas'
          } else if (urlCompleta.includes(317)) {
            document.querySelector(
              `.vtex-search-result-3-x-galleryTitle--layout`
            ).innerHTML = 'New Arrivals'
          }

          // console.log("COLECCIONES")

          // BARRA DE BUSQUEDA
          // ------------------------------------------------------------------------------------------------
        } else if (
          urlCompleta.includes('%20') ||
          urlCompleta.includes('map=ft') ||
          urlCompleta.includes('fuzzy')
        ) {
          const ls = document.querySelectorAll(
            `div[data-testid="breadcrumb"] > span > a`
          )
          // console.log(ls[0].innerText, "busqueda")

          for (let i = 0; i < ls.length; i++) {
            if (ls[i].innerText === 'Si') {
              document.querySelector(
                `.vtex-search-result-3-x-galleryTitle--layout`
              ).innerHTML = ls[0].innerText
              // console.log(ls[0].innerText, "barra de busqueda con descuento")
              break
            } else {
              document.querySelector(
                `.vtex-search-result-3-x-galleryTitle--layout`
              ).innerHTML = ls[0].innerText
              // console.log(ls[0].innerText, "barra de busqueda sin descuento")
              break
            }
          }

          // console.log("BUSQUEDA")
        }
      } else {
        console.log('no hacer nada')
      }

      // ---------------------------------------------------------------------------------------------------------
      // breadcrumb
      const ls = document.querySelectorAll(
        `div[data-testid="breadcrumb"] > span > a`
      )
      for (let i = 0; i < ls.length; i++) {
        if (ls[i].innerText === '/si') {
          ls[i].innerText = 'Sale'
          break
        }
      }
    }, 500)
  })
  return null
}

export default RenameFilter
