import React, { useState, useEffect } from 'react'

const CorebizCloseModalQuickview = () => {
  useEffect(() => {
    document
      .querySelector(
        '.vtex-flex-layout-0-x-flexRowContent--buy-button-quickview'
      )
      .addEventListener('click', () => {
        setTimeout(() => {
          document
            .querySelector(
              '.vtex-modal-layout-0-x-closeButton--close-quickview'
            )
            .click()
        }, 1000)
      })
  })

  return null
}

export default CorebizCloseModalQuickview
