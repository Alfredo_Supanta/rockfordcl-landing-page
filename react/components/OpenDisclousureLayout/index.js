import React, { useEffect } from 'react'

const OpenDisclousureLayout = () => {
  useEffect(() => {
    if (document.querySelector('a[href*="/perfecto-estado"]')) {
      const linkToPerfectoEstado = document.querySelectorAll(
        'a[href*="/perfecto-estado"]'
      )
      linkToPerfectoEstado.forEach((link) => {
        link.addEventListener('click', (e) => {
          e.preventDefault()
          e.stopImmediatePropagation()
          openAndGo(link)
        })
      })
    }

    if (document.querySelector('a[href*="/embalaje-original"]')) {
      const linkToPerfectoEstado = document.querySelectorAll(
        'a[href*="/embalaje-original"]'
      )
      linkToPerfectoEstado.forEach((link) => {
        link.addEventListener('click', (e) => {
          e.preventDefault()
          e.stopImmediatePropagation()
          openAndGo(link)
        })
      })
    }

    const openAndGo = (el) => {
      // console.log(el.pathname.replace('/', ''));
      document
        .querySelector(
          `.vtex-disclosure-layout-1-x-trigger--${el.pathname.replace('/', '')}`
        )
        .click()
      document
        .querySelector(
          `.vtex-disclosure-layout-1-x-trigger--${el.pathname.replace('/', '')}`
        )
        .scrollIntoView({ behavior: 'smooth', block: 'center' })
    }
  }, [])

  return null
}

export default OpenDisclousureLayout
