import React from 'react'

export default function Fragmen({
  children = null,
  useDiv = false,
  blockClass = undefined,
  className = '',
} = {}) {
  if (useDiv || blockClass)
    return (
      <div
        className={`${
          blockClass ? `vtex-fragmen-${blockClass}` : undefined
        } ${className}`}
      >
        {children}
      </div>
    )

  return children
}
