# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [8.0.49] - 2022-10-20

## [8.0.48] - 2022-10-19

## [8.0.47] - 2022-10-18

## [8.0.46] - 2022-10-17

## [8.0.45] - 2022-10-12

## [8.0.44] - 2022-10-06

## [8.0.43] - 2022-10-06

## [8.0.42] - 2022-10-03

## [8.0.41] - 2022-10-03

## [8.0.40] - 2022-10-02

## [8.0.39] - 2022-09-29

## [8.0.38] - 2022-09-28

## [8.0.37] - 2022-09-28

## [8.0.36] - 2022-09-28

## [8.0.35] - 2022-09-27

## [8.0.34] - 2022-09-27

## [8.0.33] - 2022-09-27

## [8.0.32] - 2022-09-27

## [8.0.31] - 2022-09-26

## [8.0.30] - 2022-09-22

## [8.0.29] - 2022-09-21

## [8.0.28] - 2022-09-21

## [8.0.27] - 2022-09-21

## [8.0.26] - 2022-09-21

## [8.0.25] - 2022-09-21

## [8.0.24] - 2022-09-20

## [8.0.23] - 2022-09-15

## [8.0.22] - 2022-09-14

## [8.0.21] - 2022-09-13

## [8.0.20] - 2022-09-06

## [8.0.19] - 2022-09-01

## [8.0.18] - 2022-08-30

## [8.0.17] - 2022-08-25

## [8.0.16] - 2022-08-24

## [8.0.15] - 2022-08-23

## [8.0.14] - 2022-08-23

## [8.0.13] - 2022-08-23

## [8.0.12] - 2022-08-08

## [8.0.10] - 2022-08-01

## [8.0.9] - 2022-07-25

## [8.0.8] - 2022-07-21

## [8.0.7] - 2022-07-21

## [8.0.6] - 2022-07-20

## [8.0.5] - 2022-07-19

## [8.0.4] - 2022-07-12

## [8.0.3] - 2022-07-04

## [8.0.2] - 2022-06-30

## [8.0.1] - 2022-06-29

## [8.0.0] - 2022-06-28

## [7.0.21] - 2022-06-28

## [7.0.20] - 2022-06-23

## [7.0.18] - 2022-06-13

## [7.0.17] - 2022-06-13

## [7.0.16] - 2022-06-13

## [7.0.15] - 2022-06-08

## [7.0.14] - 2022-06-07

## [7.0.13] - 2022-06-06

## [7.0.12] - 2022-06-02

## [7.0.11] - 2022-05-27

## [7.0.10] - 2022-05-26

## [7.0.9] - 2022-05-26

## [7.0.8] - 2022-05-25

## [7.0.7] - 2022-05-25

## [7.0.6] - 2022-05-24

## [7.0.5] - 2022-05-24

## [7.0.4] - 2022-05-20

## [7.0.3] - 2022-05-19

## [7.0.2] - 2022-05-19

## [7.0.1] - 2022-05-19

## [7.0.0] - 2022-05-19

## [6.0.67] - 2022-05-19

## [6.0.66] - 2022-05-18

## [6.0.65] - 2022-05-18

## [6.0.64] - 2022-05-17

## [6.0.63] - 2022-05-17

## [6.0.62] - 2022-05-16

## [6.0.61] - 2022-05-16

## [6.0.60] - 2022-05-12

## [6.0.59] - 2022-05-11

## [6.0.58] - 2022-05-11

## [6.0.57] - 2022-05-10

## [6.0.56] - 2022-05-10

## [6.0.55] - 2022-05-10

## [6.0.54] - 2022-05-09

## [6.0.53] - 2022-05-04

## [6.0.52] - 2022-05-03

## [6.0.51] - 2022-04-28

## [6.0.50] - 2022-04-27

## [6.0.49] - 2022-04-26

## [6.0.48] - 2022-04-25

## [6.0.47] - 2022-04-18

## [6.0.46] - 2022-04-18

## [6.0.45] - 2022-04-18

## [6.0.44] - 2022-04-13

## [6.0.43] - 2022-04-13

## [6.0.42] - 2022-04-01

## [6.0.41] - 2022-03-24

## [6.0.40] - 2022-03-23

## [6.0.39] - 2022-03-17

## [6.0.38] - 2022-03-07

## [6.0.37] - 2022-03-07

## [6.0.36] - 2022-03-01

## [6.0.35] - 2022-02-28

## [6.0.34] - 2022-02-23

## [6.0.33] - 2022-02-21

## [6.0.32] - 2022-02-15

## [6.0.31] - 2022-02-14

## [6.0.30] - 2022-02-10

## [6.0.29] - 2022-02-07

## [6.0.28] - 2022-01-27

## [6.0.27] - 2022-01-27

## [6.0.26] - 2022-01-26

## [6.0.25] - 2022-01-25

## [6.0.24] - 2022-01-24

## [6.0.23] - 2022-01-20

## [6.0.22] - 2022-01-20

## [6.0.21] - 2022-01-12

## [6.0.20] - 2022-01-12

## [6.0.19] - 2022-01-12

## [6.0.18] - 2022-01-10

## [6.0.17] - 2022-01-03

## [6.0.16] - 2022-01-03

## [6.0.15] - 2021-12-28

## [6.0.14] - 2021-12-27

## [6.0.13] - 2021-12-16

## [6.0.12] - 2021-12-15

## [6.0.11] - 2021-12-14

## [6.0.10] - 2021-12-14

## [6.0.9] - 2021-12-13

## [6.0.8] - 2021-11-30

## [6.0.7] - 2021-11-24

## [6.0.6] - 2021-11-24

## [6.0.5] - 2021-11-24

## [6.0.4] - 2021-11-24

## [6.0.3] - 2021-11-24

## [6.0.2] - 2021-11-24

## [6.0.1] - 2021-11-23

## [6.0.0] - 2021-11-23

## [5.0.5] - 2021-11-23

## [5.0.4] - 2021-11-22

## [5.0.3] - 2021-11-18

## [5.0.2] - 2021-11-17

## [5.0.1] - 2021-11-17

## [5.0.0] - 2021-11-15

## [4.0.20] - 2021-11-15

## [4.0.19] - 2021-11-08

## [4.0.18] - 2021-11-08

## [4.0.17] - 2021-11-02

## [4.0.16] - 2021-10-27

## [4.0.15] - 2021-10-27

## [4.0.14] - 2021-10-26

## [4.0.13] - 2021-10-20

## [4.0.12] - 2021-10-18

## [4.0.11] - 2021-10-14

## [4.0.10] - 2021-10-13

## [4.0.9] - 2021-10-08

## [4.0.8] - 2021-10-05

## [4.0.7] - 2021-10-01

## [4.0.6] - 2021-09-30

## [4.0.5] - 2021-09-29

## [4.0.4] - 2021-09-29

## [4.0.3] - 2021-09-29

## [4.0.2] - 2021-09-27

## [4.0.1] - 2021-09-27

## [4.0.0] - 2021-09-24

## [3.0.52] - 2021-09-20

## [3.0.51] - 2021-09-06

## [3.0.50] - 2021-09-01

## [3.0.49] - 2021-09-01

## [3.0.48] - 2021-09-01

## [3.0.49] - 2021-08-31

## [3.0.48] - 2021-08-26

## [3.0.47] - 2021-08-25

## [3.0.46] - 2021-08-24

## [3.0.45] - 2021-08-18

## [3.0.44] - 2021-08-18

## [3.0.43] - 2021-08-18

## [3.0.42] - 2021-08-16

## [3.0.41] - 2021-08-16

## [3.0.40] - 2021-08-12

## [3.0.39] - 2021-08-02

## [3.0.38] - 2021-07-27

## [3.0.37] - 2021-07-22

## [3.0.36] - 2021-07-07

## [3.0.35] - 2021-07-07

## [3.0.34] - 2021-07-06

## [3.0.33] - 2021-07-06

## [3.0.32] - 2021-06-29

## [3.0.31] - 2021-06-18

## [3.0.30] - 2021-06-16

## [3.0.29] - 2021-06-16

## [3.0.28] - 2021-06-08

## [3.0.27] - 2021-06-08

## [3.0.26] - 2021-06-08

## [3.0.25] - 2021-06-07

## [3.0.24] - 2021-06-03

## [3.0.23] - 2021-05-31

## [3.0.22] - 2021-05-28

## [3.0.21] - 2021-05-28

## [3.0.20] - 2021-05-12

## [3.0.19] - 2021-05-04

## [3.0.18] - 2021-04-28

## [3.0.17] - 2021-04-19

## [3.0.16] - 2021-04-15

## [3.0.15] - 2021-04-14

## [3.0.14] - 2021-04-13

## [3.0.13] - 2021-04-12

## [3.0.12] - 2021-04-12

## [3.0.11] - 2021-04-09

## [3.0.10] - 2021-04-09

## [3.0.9] - 2021-04-05

## [3.0.8] - 2021-03-25

## [3.0.7] - 2021-03-25

## [3.0.6] - 2021-03-18

## [3.0.5] - 2021-03-17

## [3.0.4] - 2021-03-09

## [3.0.3] - 2021-03-09

## [3.0.2] - 2021-02-25

## [3.0.1] - 2021-02-25

## [3.0.0] - 2021-02-25

## [2.0.15] - 2021-02-16

## [2.0.14] - 2021-02-11

## [2.0.13] - 2021-02-10

## [2.0.12] - 2021-02-04

## [2.0.11] - 2021-01-29

## [2.0.10] - 2021-01-27

## [2.0.9] - 2021-01-22

## [2.0.8] - 2021-01-20

## [2.0.7] - 2021-01-20

## [2.0.6] - 2021-01-18

## [2.0.5] - 2021-01-08

## [2.0.4] - 2021-01-07

## [2.0.3] - 2021-01-06

## [2.0.2] - 2021-01-06

## [2.0.1] - 2020-12-31

## [2.0.0] - 2020-12-30

## [1.2.69] - 2020-12-30

## [1.2.68] - 2020-12-30

## [1.2.67] - 2020-12-30

## [1.2.66] - 2020-12-28

## [1.2.65] - 2020-12-28

## [1.2.64] - 2020-12-28

## [1.2.63] - 2020-12-28

## [1.2.62] - 2020-12-23

## [1.2.61] - 2020-12-23

## [1.2.60] - 2020-12-23

## [1.2.59] - 2020-12-23

## [1.2.58] - 2020-12-23

## [1.2.57] - 2020-12-23

## [1.2.56] - 2020-12-23

## [1.2.55] - 2020-12-16

## [1.2.54] - 2020-12-14

## [1.2.53] - 2020-12-14

## [1.2.52] - 2020-12-09

## [1.2.51] - 2020-12-09

## [1.2.50] - 2020-12-09

## [1.2.49] - 2020-12-04

## [1.2.48] - 2020-12-04

## [1.2.47] - 2020-12-04

## [1.2.46] - 2020-11-26

## [1.2.45] - 2020-11-26

## [1.2.44] - 2020-11-24

## [1.2.43] - 2020-11-24

## [1.2.42] - 2020-11-20

## [1.2.41] - 2020-11-19

## [1.2.40] - 2020-11-18

## [1.2.39] - 2020-11-18

## [1.2.38] - 2020-11-17

## [1.2.37] - 2020-11-13

## [1.2.36] - 2020-11-12

## [1.2.35] - 2020-11-10

## [1.2.34] - 2020-11-10

## [1.2.33] - 2020-11-09

## [1.2.32] - 2020-11-09

## [1.2.31] - 2020-11-05

## [1.2.30] - 2020-11-03

## [1.2.29] - 2020-11-02

## [1.2.28] - 2020-11-01

## [1.2.27] - 2020-11-01

## [1.2.26] - 2020-10-28

## [1.2.25] - 2020-10-27

## [1.2.24] - 2020-10-26

## [1.2.23] - 2020-10-23

## [1.2.22] - 2020-10-22

## [1.2.21] - 2020-10-22

## [1.2.20] - 2020-10-21

## [1.2.19] - 2020-09-28

## [1.2.18] - 2020-09-24

## [1.2.17] - 2020-09-23

## [1.2.16] - 2020-09-15

## [1.2.15] - 2020-09-14

## [1.2.14] - 2020-09-08

## [1.2.13] - 2020-09-03

## [1.2.12] - 2020-09-03

## [1.2.11] - 2020-09-02

## [1.2.10] - 2020-09-01

## [1.2.9] - 2020-08-30

## [1.2.8] - 2020-08-30

## [1.2.7] - 2020-08-30

## [1.2.6] - 2020-08-30

## [1.2.5] - 2020-08-30

## [1.2.4] - 2020-08-30

## [1.2.3] - 2020-08-28

## [1.2.2] - 2020-08-26

## [1.2.1] - 2020-08-26

## [1.2.0] - 2020-08-21

## [1.1.12] - 2020-08-21

## [1.1.11] - 2020-08-20

## [1.1.10] - 2020-08-19

## [1.1.9] - 2020-08-14

## [1.1.8] - 2020-07-31

## [1.1.7] - 2020-07-31

## [1.1.6] - 2020-07-30

## [1.1.5] - 2020-07-24

## [1.1.4] - 2020-07-22

## [1.1.3] - 2020-07-22

## [1.1.2] - 2020-07-21

## [1.1.1] - 2020-07-21

## [1.1.0] - 2020-07-20

## [1.0.24] - 2020-07-15

## [1.0.23] - 2020-07-10

## [1.0.22] - 2020-07-10

## [1.0.21] - 2020-07-06

## [1.0.20] - 2020-07-06

## [1.0.19] - 2020-07-06

## [1.0.18] - 2020-07-05

## [1.0.17] - 2020-07-01

## [1.0.16] - 2020-06-24

## [1.0.15] - 2020-06-22

## [1.0.14] - 2020-06-12

## [1.0.13] - 2020-06-09

## [1.0.12] - 2020-06-08

## [1.0.11] - 2020-06-08

## [1.0.10] - 2020-06-05

## [1.0.9] - 2020-06-05

## [1.0.8] - 2020-06-03

## [1.0.7] - 2020-05-28

## [1.0.6] - 2020-05-27

## [1.0.5] - 2020-05-21

## [1.0.4] - 2020-05-12

## [1.0.3] - 2020-05-11

## [1.0.2] - 2020-05-05

## [1.0.1] - 2020-05-05

## [1.0.0] - 2020-04-01

## [0.1.31] - 2020-03-20

## [0.1.30] - 2020-03-20

## [0.1.29] - 2020-03-20

## [0.1.0] - 2020-01-10

## [0.0.2] - 2019-12-30

## [3.21.1] - 2019-12-27
### Fixed
- Use docs builder.

## [3.21.0] - 2019-12-20
### Added
- `showValueNameForImageVariation` to `sku-selector`.

### Changed
- Remove product-identifier.

## [3.20.2] - 2019-12-19
### Fixed
- Menu links and layout

## [3.20.1] - 2019-12-18
### Changed
- Use `styles-builder@2.x`.

## [3.20.0] - 2019-12-17
### Changed
- Use new flexible `minicart.v2` and `add-to-cart-button`.

### Added
- Custom CSS styles for `product-identifier`.

## [3.20.0-beta.0] - 2019-12-11

## [3.20.0-beta] - 2019-12-06

## [3.19.2] - 2019-12-16
### Fixed
- `minItemsPerPage` prop in `shelf#home` block.

## [3.19.1] - 2019-12-03

## [3.18.2] - 2019-12-03
### Fixed
- Add missing dependencies

## [3.18.1] - 2019-11-11
### Fixed
- Use the proper API to space SKU Selector

## [3.18.0] - 2019-11-11
### Fixed
- Product page spacing issues.

### Added
- Product description.

## [3.17.2] - 2019-11-08
### Added
- Use `skusFilter` `FIRST_AVAILABLE` value.

## [3.17.1] - 2019-11-06
### Fixed
- Remove usage of deprecated selectors.

## [3.17.0] - 2019-11-06
### Changed
- PreventRouteChange to `true`.

## [3.16.2] - 2019-10-17

### Changed

- Default font.

## [3.16.1] - 2019-10-08

## [3.16.0] - 2019-10-07

### Added

- The `search-fetch-previous` block to the search result.

## [3.15.1] - 2019-09-23

## [3.15.0] - 2019-09-18

### Added

- Add sitemap builder with about-us url

## [3.14.0] - 2019-09-18

### Added

- Product Customizer to PDP.

## [3.13.1] - 2019-09-10

### Fixed

- Use `search-fetch-more`.

## [3.13.0] - 2019-09-10

### Changed

- Use flexble layout for `search-result`.

## [3.12.0] - 2019-08-27

### Added

- Accordion menu to footer on mobile.

## [3.11.0] - 2019-08-20

### Changed

- Use `flex-layout` to define the `footer` block.

### Fixed

- Missing padding in the Footer.

## [3.10.0] - 2019-08-16

### Removed

- `product-add-to-list-button` from `flex-layout.col#product-image` so that it isn't rendered in the products page.

### Added

- New props (`minItemsPerPage` and `paginationDotsVisibility`) for the Shelf component to `shelf#home`.

## [3.9.1] - 2019-08-14

### Fixed

- Remove incorrect props from search-result block.

## [3.9.0] - 2019-08-07

### Added

- created a `breadcrumb` block with `showOnMobile` set to true

## [3.8.0] - 2019-08-01

### Added

- `mobileLayout` prop to `search-result` block.

## [3.7.2] - 2019-07-31

### Fixed

- Add product-review-form block to avoid falling back to the default layout.

## [3.7.1] - 2019-07-26

### Changed

- fixed some errors in the category-menu

## [3.7.0] - 2019-07-23

### Added

- `displayThumbnailsArrows` to the `product-images` block.

## [3.6.1] - 2019-07-17

### Changed

- Split the blocks.json into multiple files.

## [3.6.0] - 2019-07-04

### Added

- Add `product-identifier.product` to the product page.
- Add `product-identifier.summary` to the product summary.

## [3.5.1] - 2019-06-12

### Fixed

- Show the heart icon of wish list in product details.

## [3.5.0] - 2019-06-11

### Added

- Product Review interfaces to PDP and shelf.

### Changed

- `product-summary` to `product-summary.shelf` so it's possible to add product review interfaces in the shelf.

## [3.4.2] - 2019-06-11

### Added

- Example of institutional page.

## [3.4.1] - 2019-06-10

### Changed

- Use new `filter-navigator`.

## [3.4.0] - 2019-06-04

## [3.3.0] - 2019-06-04

### Changed

- Changed logo position in header.

## [3.3.0] - 2019-06-04

### Changed

- Product details is now broken down into smaller blocks, inserted directly into `store.product`.

## [3.2.1] - 2019-05-28

### Fixed

- `labelListPrice` and `labelSellingPrice` defaults.

## [3.2.0] - 2019-05-28

### Added

- `LocaleSwitcher` component to the `Header`.

## [3.1.1] - 2019-05-27

## [3.1.0] - 2019-05-25

### Fixed

- Changed the way props are declared in product-summary and product-details.

### Changed

- New store layout using flexible blocks for Header and Footer.

## [2.4.1] - 2019-05-17

### Added

- Add `under construction` status to product kit

## [2.4.0] - 2019-05-09

### Added

- Add `labelListPrice` in product-details and summary blocks.

## [2.3.1] - 2019-05-06

## [2.3.0] - 2019-03-27

### Added

- Add `product-highlights` in `product-details#default` block.

## [2.2.2] - 2019-05-02

- Add `store.orderplaced` block definition to `blocks.json`.

## [2.2.1] - 2019-03-18

### Fixed

- Include missing dependencies. Previously, it was working only due to a dependency leak on IO, but the store-theme was breaking since that problem was fixed.

## [2.2.0] - 2019-02-18

### Changed

- Update app name to `store-theme` instead of `dreamstore`.
- Change `related-products` for `shelf.relatedProducts`.

## [2.1.0] - 2019-02-12

### Added

- Add product-specifications in product-details block.

## [2.0.2] - 2019-02-05

### Fixed

- Moved hard-coded store version dependency from 2.0.0 to 2.x

## [2.0.1] - 2019-02-05

### Added

- Add new required blocks for `ProductDetail`.

## [2.0.0] - 2019-02-01

### Added

- Add profile challenge block on account.
- Bye `pages.json`! Welcome `store-builder`.
- Add styles builder 1.x
- Two new nav icons.
- New Icon for telemarketing.
- Default padding setted on body.
- Dreamstore with Design Tokens! :tada

### Changed

- Configure blocks props.
- Remove `global.css` and bump `vtex.store` to 2.0.0.
- Adjust search-result blocks configuration.

## [1.18.6] - 2018-12-20

### Fixed

- Remove Fabriga font from global.css.

## [1.18.5] - 2018-11-23

### Changed

- Update Search Result icons.

## [1.18.4] - 2018-11-23

### Changed

- Update Profile and Minicart Icon.

## [1.18.3] - 2018-11-07

### Fixed

- Fix paddings to match header.

## [1.18.2] - 2018-11-01

### Added

- `IconPack` component to serve the icon used by the dreamstore components.

## [1.18.1] - 2018-10-25

## [1.18.0] - 2018-10-02

### Added

- Component definitions for `vtex.search-result` new extension points.

## [1.17.0] - 2018-10-02

### Removed

- Remove unused queries and tests.

## [1.16.0] - 2018-09-26

### Changed

- Import footer from the new app `vtex.dreamstore-footer`.

## [1.15.2] - 2018-09-20

### Fixed

- Remove Category Menu CSS class definition.

### Changed

- Remove `package-lock.json` from react/ folder.

## [1.15.1] - 2018-09-19

### Changed

- Moved product details breadcrumb to be inside of the `ProductDetails`.

## [1.15.0] - 2018-09-18

### Added

- `Header` standalone component.

## [1.14.1] - 2018-09-18

### Added

- Add again Telemarketing app to the Header extension point.

## [1.14.0] - 2018-09-14

### Added

- `Logo` and `SearchBar` as extensions of the `Header`.

## [1.13.3] - 2018-09-12

### Added

- Page padding class to allow apps to have same default padding.

### Removed

- Unused dependency `vtex.product-summary`

## [1.13.2] - 2018-09-05

### Fixed

- **HotFix** Remove telemarketing app from dreamstore.

## [1.13.1] - 2018-09-05

### Fixed

- Fix malformed release.

## [1.13.0] - 2018-09-05

### Added

- Add `vtex.my-account` app.

## [1.12.2] - 2018-08-30

### Changed

- Bump version of `vtex.store-components` and `vtex.styleguide`.

## [1.12.1] - 2018-08-24

### Fixed

- Fix carousel position in home page.

## [1.12.0] - 2018-08-24

### Changed

- Renamed `SearchResult` to `index`.
- Bumped `vtex.search-result` to version 1.x.

## [1.11.0] - 2018-08-24

### Changed

- Bump major of `vtex.category-menu`.

## [1.10.5] - 2018-08-17

### Changed

- Update `menu` and `minicart` versions to 1.x.

## [1.10.4] - 2018-08-17

## [1.10.3] - 2018-08-16

### Fixed

- Rollback rename `CategoriesHighlights`.

## [1.10.2] - 2018-08-16

### Changed

- Undeprecate v1.10.1.

## [1.10.1] - 2018-08-15

### Changed

- `CategoriesHighlights` to `CategoriesHightlighted`.
- Bump `vtex.telemarketing` to 1.x.

## [1.10.0] - 2018-08-14

## [1.9.5] - 2018-08-13

### Added

- Component `ImpersonateCustomer` to `Header`.
- DepartmentHeader component to wrap Carousel and MainCategories components.
- Department page template.

## [1.9.4] - 2018-08-02

### Changed

- Bump `vtex.styleguide` major version.

## [1.9.3] - 2018-07-30

### Fixed

- Fix the `my-orders` rendering error.

## [1.9.2] - 2018-07-27

### Changed

- Update `vtex.login` version.

## [1.9.1] - 2018-07-24

### Changed

- Bump my-orders version dependency to use stable.

## [1.7.0] - 2018-07-09

### Added

- Loading extension to pages.json

## [1.6.0] - 2018-7-6

### Added

- Add ProductKit to pages.json

## [1.5.1] - 2018-7-6

### Removed

- Moved `store/login/container` to `vtex.store`.

## [1.5.0] - 2018-7-6

### Added

- `vtex.login/LoginContent` to `store/login/container` extension point.

## [1.4.0] - 2018-7-6

### Added

- Add `SearchResult` to the brand page.

## [1.3.2] - 2018-7-4

### Changed

- Use `store-components/Header` instead internal component `Header`.

## [1.3.1] - 2018-6-27

### Changed

- `pages.json` to inject `search-result` into `CategoryPage`

### Fixed

- Remove the integration with `vtex.my-orders-app`.

## [1.3.0] - 2018-6-20

### Added

- Add `vtex.shelf/RelatedProducts` component to the product page.

## [1.2.2] - 2018-6-15

### Fixed

- Fix incorrect build made by builder-hub

## [1.2.1] - 2018-6-14

### Fixed

- Fix my-orders-app version in manifest.json

## [1.2.0] - 2018-6-14

### Added

- Add integration with `vtex.search-result`.
- Add integration with `vtex.my-orders-app`.

### Removed

- Remove dependency `vtex.gallery`.
- Remove `GalleryWrapper` component.
- Remove file `queries/productsQuery.gql`.

### Fixed

- Adapt Top Menu CSS to integrate `vtex.login`.

## [1.1.0] - 2018-6-8

### Added

- _Login_ component to the _Header_.
- Add `vtex.styleguide` dependency.

## [1.0.0] - 2018-6-4

### Added

- Add the breadcrumb component to the SearchPage and ProductPage

### Fixed

- Removed redundant Spinner in _ProductPage_ Component.

### Added

- **Breaking Change** Now, dreamstore-theme is a template based on `vtex.store`.
- Add free billing policy in `manifest.json`.
- Add the breadcrumb component to the `SearchPage` and `ProductPage`.

### Changed

- Changed `postreleasy` script to publish only on vtex vendor.

## [0.3.5] - 2018-05-21

### Fixed

- Fix pages dependency to be able to use `ExntesionContainer` again.
- `Topbar` when scrolled overlapped the `VTEX-topbar`.
- Update css product details class for the spinner be in the center.

## [0.3.4] - 2018-05-19

### Changed

- Update version of `vtex.storecomponents` to 1.x

## [0.3.3] - 2018-05-18

### Added

- Add toast message system to be used on error scenarios.

### Fixed

- Fix padding top of product page content

## [0.3.2] - 2018-05-18

### Fixed

- Top menus covering great portion of the page.
- Fix pages error when ExtensionContainer was used.

## [0.3.1] - 2018-05-12

### Fixed

- Display category menu only in large screens.
- Fix padding-top of Product page.

## [0.3.0] - 2018-05-12

### Added

- Add category menu and fix padding.
- Add the search bar component and make header responsive again.

### Fixed

- Fix minicart div position
- Remove flex box from product page to fix non-expected behavior of react-slick

## [0.2.0] - 2018-05-11

### Added

- Show success toast when a product is add to the cart.
- Add responsive layout to the header.
- Add gallery to the search page.

## [0.1.0] - 2018-05-11

### Added

- Add the search bar component

### Changed

- Replace own Footer implementation by `vtex.storecomponents/Footer` component.

## [0.0.11] - 2018-05-09

### Added

- Add Product Details app.

### Deprecated

- Remove legacy implementations of buy button and minicart.

## [0.0.10] - 2018-05-09

### Added

- Add Minicart app.

## [0.0.9] - 2018-05-07

### Added

- Add Menu app on top bar.

### Deprecated

- Remove the own implementation of shelf to add the app.
