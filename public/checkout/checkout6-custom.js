/* eslint no-undef: 0, prefer-rest-params: 0, no-unused-vars: 0, radix: 0, no-restricted-globals: 0, eqeqeq: 0,  vars-on-top: 0, no-var: 0 */
window.onload = function () {
  shippingDay()
  lenghtInput()
  termsCond()

  async function setCart(itemsCart) {
    const items = await itemsCart.map((item) => {
      return {
        ...item,
        price_currency_code: 'CLP',
        product_id: item.productRefId || item.productId,
        unit_price: item.price || item.sellingPrice,
      }
    })

    const nostojs = window.nostojs || null

    if (nostojs) {
      nostojs((api) => {
        api.defaultSession().setCart({ items })
      })
    }
  }

  setCart(vtexjs.checkout.orderForm.items)
}

const s = document.createElement('script')
s.type = 'text/javascript'
s.src = '//connect.nosto.com/include/dw3sjtna'
$('head').append(s)
;(function () {
  const name = 'nostojs'
  window[name] =
    window[name] ||
    function (cb) {
      ;(window[name].q = window[name].q || []).push(cb)
    }
})()

window.onhashchange = function () {
  barProgress()
  shippingDay()
  lenghtInput()
  termsCond()
  img()
  totalCard()
}

$(window).on('orderFormUpdated.vtex', function (evt, orderForm) {
  const { clientProfileData, userProfileId, items, storePreferencesData } =
    orderForm

  if (
    clientProfileData &&
    clientProfileData.email &&
    clientProfileData.firstName &&
    clientProfileData.lastName &&
    userProfileId
  ) {
    $('.nosto_customer').remove()

    $('head').append(`
          <div class="nosto_customer" style="display: none">
                    <span class="nosto_page_type">Checkout</span>
                  <span class="email">${clientProfileData.email}</span>
                  <span class="first_name">${clientProfileData.firstName}</span>
                  <span class="last_name">${clientProfileData.lastName}</span>
                  <span class="customer_reference">${userProfileId}</span>
                  <span class="marketing_permission">false</span>
              </div>
      `)
  }

  $('.nosto_cart').remove()

  $('head').append(`
          <div class="nosto_cart" style="display: 'none'">
                  ${items.map((item) => {
                    return `<div class="line_item">
                            <span class="nosto_page_type">Checkout</span>
                          <span class="product_id">${item.productId}</span>
                          <span class="sku_id">${item.id}</span>
                            <span class="quantity">${item.quantity}</span>
                          <span class="name">${item.name}</span>
                          <span class="unit_price">${
                            item.price || item.sellingPrice
                          }</span>
                <span class="nosto_page_type">Checkout</span>
                            <span class="price_currency_code">${
                              storePreferencesData.currencyCode
                            }</span>
                          </div>`
                  })}
              </div>
      `)

  barProgress()
  shippingDay()
  lenghtInput()
  termsCond()
  img()
  cupom()
  totalCard()

  if (
    document.getElementById('cart-title') &&
    document.getElementById('cart-title').children[0] &&
    document.getElementById('cart-title').children[0].children[0]
  ) {
    let text = ' item en el carro'
    let textContentNumber = document
      .getElementById('cart-title')
      .children[0].children[0].textContent.split(' ')[0]
    textContentNumber = parseInt(textContentNumber)
    if (textContentNumber > 1) {
      text = ' items en el carro'
    }
    document.getElementById('cart-title').children[0].children[0].textContent =
      textContentNumber + text
  }
})

$(window).on('checkoutRequestBegin.vtex', function (evt, orderForm) {
  barProgress()
  shippingDay()
  lenghtInput()
  termsCond()
  img()
  totalCard()
})

$(window).on('checkoutRequestEnd.vtex', function (evt, orderForm) {
  barProgress()
  shippingDay()
  lenghtInput()
  termsCond()
  img()
  totalCard()
})

$(window).on('componentValidated.vtex', function (event, orderForm) {
  shippingDay()
  lenghtInput()
  termsCond()
})

$(document).ready(function () {
  $('#barProgress').insertBefore($('#order-by'))
})

function img() {
  if (location.hash == '#/cart') {
    setTimeout(function () {
      $('.product-item img').each(function () {
        $(this).attr('src', $(this).attr('src').replace('-75-56/', '-120-120/'))
      })
    }, 400)
  }
}
function cupom() {
  if ($('.cart-template').hasClass('active')) {
    var force = setInterval(function () {
      $('.coupon-fieldset .link-coupon-add').trigger('click')
      clearInterval(force)
    }, 500)
  }
}
function barProgress() {
  $('#barProgress li').each(function () {
    $(this).removeClass('active')
    if (location.hash == '#/payment') {
      if ($(this).hasClass('item-payment')) {
        $(this).addClass('active')
      }
    }
    if (location.hash == '#/profile') {
      if ($(this).hasClass('item-shipping')) {
        $(this).addClass('active')
      }
    }
    if (location.hash == '#/shipping') {
      if ($(this).hasClass('item-shipping')) {
        $(this).addClass('active')
      }
    }
    if (location.hash == '#/cart') {
      if ($(this).hasClass('item-cart')) {
        $(this).addClass('active')
      }
    }
  })
}

function totalCard() {
  let qtd = 0

  for (let i = 0; i < window.vtexjs.checkout.orderForm.items.length; i++) {
    qtd += parseFloat(window.vtexjs.checkout.orderForm.items[i].quantity)
  }

  if ($('#cart-title small').length < 1) {
    $('#cart-title').append('<small>' + qtd + ' items in your cart</small>')
  } else {
    $('#cart-title small').html('<small>' + qtd + ' items in your cart</small>')
  }
}

function shippingDay() {
  if ($('.shp-summary-package-time span').length) {
    const text1 = $('.shp-summary-package-time span')
      .text()
      .replace('En', 'Desde 7')

    $('.shp-summary-package-time span').text(text1)
  }

  if ($('.vtex-omnishipping-1-x-leanShippingTextLabelSingle span').length) {
    const text2 = $('.vtex-omnishipping-1-x-leanShippingTextLabelSingle span')
      .text()
      .replace('En', 'Desde 7')

    $('.vtex-omnishipping-1-x-leanShippingTextLabelSingle span').text(text2)
  }

  if ($('#show-gift-card-group').length) {
    const text2 = $('#show-gift-card-group')
      .text()
      .replace('un cupón de regalo', 'Giftcard')

    $('#show-gift-card-group').text(text2)
  }
}

function lenghtInput() {
  $('#ship-street').attr('maxlength', 60)
  $('#ship-receiverName').attr('maxlength', 60)
  $('#ship-number').attr('maxlength', 60)
  $('#ship-complement').attr('maxlength', 60)
}

function termsCond() {
  const $htmlBtn =
    '<p class="terms-cond">Al hacer click en Finalizar compra, estás aceptando nuestros <a href="/terminos-condiciones" target="_blank">Términos y Condiciones</a>'

  if (
    $('.clearfix.pull-right.cart-links.cart-links-bottom.hide').length == 1 &&
    $('.terms-cond').length < 1
  ) {
    $('.clearfix.pull-right.cart-links.cart-links-bottom.hide').append($htmlBtn)
  }
}

// Hotjar
;(function (h, o, t, j, a, r) {
  const siteId = '1072156'
  h.hj =
    h.hj ||
    function () {
      ;(h.hj.q = h.hj.q || []).push(arguments)
    }
  h._hjSettings = { hjid: siteId, hjsv: 6 }
  a = o.getElementsByTagName('head')[0]
  r = o.createElement('script')
  r.async = 1
  r.src = t + h._hjSettings.hjid + j + h._hjSettings.hjsv
  a.appendChild(r)
})(window, document, 'https://static.hotjar.com/c/hotjar-', '.js?sv=')
